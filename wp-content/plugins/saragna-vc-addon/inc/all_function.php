<?php
add_action('init','sa_vc_team_register_post_type',0);
add_action( 'init', 'sa_vc_team_create_team_taxonomies', 0 );
add_action('init','sa_vc_testimonial_register_post_type',0);
add_action( 'init', 'sa_vc_testimonial_create_testimonial_taxonomies', 0 );
add_action('wp_head','sa_vc_register_style_script');
function sa_vc_register_style_script(){
	// all css register here
	wp_enqueue_style( 'sa-vc-font-awesome-css', plugins_url('../assets/css/font-awesome.min.css', __FILE__));
	wp_enqueue_style( 'sa-vc-bootstrap-css', plugins_url('../assets/css/bootstrap.css', __FILE__));
	wp_enqueue_style( 'sa-vc-general-css', plugins_url('../assets/css/general.css', __FILE__));
	
	wp_register_style( 'sa-vc-animate-css', plugins_url('../assets/css/animate.css', __FILE__));
	wp_register_style( 'sa-vc-slick-css', plugins_url('../assets/css/slick.css', __FILE__));	
	wp_register_style( 'sa-vc-megnific-css', plugins_url('../assets/css/magnific-popup.css', __FILE__));
	
	//animated text css
	wp_register_style( 'sa-vc-bootstrap-o-css', plugins_url('../assets/css/bootstrap.min.css', __FILE__));
	
	// all js register here.
	wp_enqueue_script('sa-vc-imagesloaded-js', plugins_url('../assets/js/imagesloaded.pkgd.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-megnific-js', plugins_url('../assets/js/megnific.js', __FILE__), array("jquery"), false, false);	
	wp_register_script('sa-vc-isotop-js', plugins_url('../assets/js/isotope.pkgd.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-carousel-js', plugins_url('../assets/js/owl.carousel.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-slick-carousel-js', plugins_url('../assets/js/slick.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-viewportchecker-js', plugins_url('../assets/js/jquery.viewportchecker.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-doT-js', plugins_url('../assets/js/doT.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-moment-locale-js', plugins_url('../assets/js/moment-with-locales.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-social-stream-js', plugins_url('../assets/js/social-stream.js', __FILE__), array("sa-vc-moment-locale-js"), false, false);
	wp_localize_script('sa-vc-social-stream-js', 'svc_ajax_url', array('url' => admin_url( 'admin-ajax.php' ),'laungage' => get_locale()));
	
	//animated text
	wp_register_script( 'sa-vc-fittext-js', plugins_url('../assets/js/jquery.fittext.js', __FILE__ ), array( 'jquery' ), false, true );
	wp_register_script( 'sa-vc-lettering-js', plugins_url('../assets/js/jquery.lettering.js', __FILE__ ), array( 'jquery' ), false, true );
	wp_register_script( 'sa-vc-textillate-js', plugins_url('../assets/js/jquery.textillate.js', __FILE__ ), array( 'jquery' ), false, true );
	//wp_register_script( 'vc-viewportchecker-js', plugins_url( ltrim( '../assets/js/jquery.viewportchecker.js', '/' ), __FILE__ ), array( 'jquery' ), false, true );
	
	//post accordion
	wp_register_script('sa-vc-bootstrap-js', plugins_url('../assets/js/bootstrap.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-bootstrap-hover-js', plugins_url('../assets/js/bootstrap-hover-dropdown.min.js', __FILE__), array("jquery"), false, false);
	
	//team
	wp_register_script('sa-vc-team-custom-js', plugins_url('../assets/js/team-custom.js', __FILE__), array("jquery"), false, true);

	//post grid and carousel
	wp_register_script('sa-vc-ddslick-js', plugins_url('../assets/js/jquery.ddslick.min.js', __FILE__), array("jquery"), false, false);
	wp_register_script('sa-vc-post-grid-script-js', plugins_url('../assets/js/post-grid-script.js', __FILE__), array("jquery"), false, false);

	//liquid tab
	wp_register_script('sa-vc-liquid-slider-js', plugins_url('../assets/js/jquery.liquid-slider.min.js', __FILE__), array("jquery"), false, true);
	wp_register_script('sa-vc-easing-js', plugins_url('../assets/js/jquery.easing.min.js', __FILE__), array("jquery"), false, true);
	wp_register_script('sa-vc-touchSwipe-js', plugins_url('../assets/js/jquery.touchSwipe.min.js', __FILE__), array("jquery"), false, true);

	//woo grid and carousel
	wp_register_script('sa-vc-woo-grid-script-js', plugins_url('../assets/js/woo-grid-script.js', __FILE__), array("jquery"), false, false);

}
add_action('init','sa_vc_woo_register_style_script');
function sa_vc_woo_register_style_script(){
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active('woocommerce/woocommerce.php') ) {
		wp_enqueue_script( 'wc-add-to-cart-variation' );
		sa_vc_woo_quick_view_action_template();
	}
}

add_action('wp_head','sa_vc_social_inline_css_for_imageloaded');
function sa_vc_social_inline_css_for_imageloaded(){
	?>
    <style>
	.svc_social_stream_container{ display:none;}
	#loader {background-image: url("<?php echo plugins_url('../addons/social-stream/css/loader.GIF',__FILE__);?>");}
	</style>
    <?php	
}

add_action('wp_ajax_svc_get_tweet','sa_vc_get_tweet');
add_action('wp_ajax_nopriv_svc_get_tweet','sa_vc_get_tweet');
function sa_vc_get_tweet(){
	require_once('twitter_proxy.php');
	extract($_POST);
	//echo "<pre>";print_r($_POST);
	// Twitter OAuth Config options
	$oauth_access_token = '531871187-jA1LUzuKOBMYy9FTHNS8Lrq3tHFtGQxCMeJMdjwY';
	$oauth_access_token_secret = '3qQgkYWzexuLoGKMnFpIoh3MZ5UEPmiRvysBBgEDIqLBn';
	$consumer_key = 'UaXiG364zfkqhkkK6ckFSRtoy';
	$consumer_secret = 'l0Ymtqh9JnuqiGULl3uvMfnqePzA03YOV9YtdAc9b6km5orW9V';
	$user_id = '78884300';
	$screen_name = $user_name;
	$count = $limit;
	
	$twitter_url = 'statuses/user_timeline.json';
	$twitter_url .= '?user_id=' . $user_id;
	$twitter_url .= '&screen_name=' . $screen_name;
	$twitter_url .= '&count=' . $count;
	if($max_id != ''){
		$twitter_url .= '&max_id=' . $max_id;
	}
	
	// Create a Twitter Proxy object from our twitter_proxy.php class
	$twitter_proxy = new TwitterProxy(
		$oauth_access_token,			// 'Access token' on https://apps.twitter.com
		$oauth_access_token_secret,		// 'Access token secret' on https://apps.twitter.com
		$consumer_key,					// 'API key' on https://apps.twitter.com
		$consumer_secret,				// 'API secret' on https://apps.twitter.com
		$user_id,						// User id (http://gettwitterid.com/)
		$screen_name,					// Twitter handle
		$count							// The number of tweets to pull out
	);
	
	// Invoke the get method to retrieve results via a cURL request
	$tweets = $twitter_proxy->get($twitter_url);
	
	echo $tweets;
wp_die();
}

add_action('wp_ajax_svc_get_search_tweet','sa_vc_get_search_tweet');
add_action('wp_ajax_nopriv_svc_get_search_tweet','sa_vc_get_search_tweet');
function sa_vc_get_search_tweet(){
	require_once('twitter_proxy.php');
	extract($_POST);
	//echo "<pre>";print_r($_POST);
	// Twitter OAuth Config options
	$oauth_access_token = '531871187-jA1LUzuKOBMYy9FTHNS8Lrq3tHFtGQxCMeJMdjwY';
	$oauth_access_token_secret = '3qQgkYWzexuLoGKMnFpIoh3MZ5UEPmiRvysBBgEDIqLBn';
	$consumer_key = 'UaXiG364zfkqhkkK6ckFSRtoy';
	$consumer_secret = 'l0Ymtqh9JnuqiGULl3uvMfnqePzA03YOV9YtdAc9b6km5orW9V';
	$user_id = '78884300';
	$screen_name = $user_name;
	$count = $limit;
	
	$twitter_url = 'search/tweets.json';
	if($other == 'yes'){
		$twitter_url .= '?q=' . $q;
		$twitter_url .= '&count=' . $limit;
		$twitter_url .= '&' . $que;
		$twitter_url .= '&include_entities' . $include_entities;
	}else{
		$twitter_url .= '?q=' . $q;
		$twitter_url .= '&count=' . $count;
	}
	
	// Create a Twitter Proxy object from our twitter_proxy.php class
	$twitter_proxy = new TwitterProxy(
		$oauth_access_token,			// 'Access token' on https://apps.twitter.com
		$oauth_access_token_secret,		// 'Access token secret' on https://apps.twitter.com
		$consumer_key,					// 'API key' on https://apps.twitter.com
		$consumer_secret,				// 'API secret' on https://apps.twitter.com
		$user_id,						// User id (http://gettwitterid.com/)
		$screen_name,					// Twitter handle
		$count							// The number of tweets to pull out
	);
	
	// Invoke the get method to retrieve results via a cURL request
	$tweets = $twitter_proxy->get($twitter_url);
	
	echo $tweets;
die();		
}

function sa_vc_post_acco_excerpt($excerpt,$limit) {
	$excerpt = strip_tags($excerpt);
	$excerpt = explode(' ', $excerpt, $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}


add_action('wp_head','sa_vc_carousel_inline_css_for_imageloaded');
function sa_vc_carousel_inline_css_for_imageloaded(){
	?>
    <style>
	.svc_post_grid_list_container{ display:none;}
	#loader {background-image: url("<?php echo plugins_url('../assets/image/loader.GIF',__FILE__);?>");}
	</style>
    <?php	
}
function sa_vc_carousel_layout_excerpt($excerpt,$limit) {
	$excerpt = explode(' ', $excerpt, $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}


//team grid start
function sa_vc_team_register_post_type(){

	// Set UI labels for Custom Post Type
		$labels = array(
			'name'                => _x( 'Teams', 'Post Type General Name', 'twentythirteen' ),
			'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'twentythirteen' ),
			'menu_name'           => __( 'Teams', 'twentythirteen' ),
			'parent_item_colon'   => __( 'Parent Team', 'twentythirteen' ),
			'all_items'           => __( 'All Teams', 'twentythirteen' ),
			'view_item'           => __( 'View Team', 'twentythirteen' ),
			'add_new_item'        => __( 'Add New Team', 'twentythirteen' ),
			'add_new'             => __( 'Add New', 'twentythirteen' ),
			'edit_item'           => __( 'Edit Team', 'twentythirteen' ),
			'update_item'         => __( 'Update Team', 'twentythirteen' ),
			'search_items'        => __( 'Search Team', 'twentythirteen' ),
			'not_found'           => __( 'Not Found', 'twentythirteen' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		
	// Set other options for Custom Post Type
		
		$args = array(
			'label'               => __( 'teams', 'twentythirteen' ),
			'description'         => __( 'Team reviews', 'twentythirteen' ),
			'labels'              => $labels,
			// Features this CPT supports in Post Editor
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			// You can associate this CPT with a taxonomy or custom taxonomy. 
			'taxonomies'          => array( 'team-category' ),
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/	
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		
		// Registering your Custom Post Type
		register_post_type( 'svc-team', $args );
}

function sa_vc_team_create_team_taxonomies(){
	$labels = array(
		'name'              => _x( 'Department', 'taxonomy general name' ),
		'singular_name'     => _x( 'Department', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Departments' ),
		'all_items'         => __( 'All Departments' ),
		'parent_item'       => __( 'Parent Department' ),
		'parent_item_colon' => __( 'Parent Department:' ),
		'edit_item'         => __( 'Edit Department' ),
		'update_item'       => __( 'Update Department' ),
		'add_new_item'      => __( 'Add New Department' ),
		'new_item_name'     => __( 'New Department Name' ),
		'menu_name'         => __( 'Department' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'department' ),
	);

	register_taxonomy( 'department', array( 'svc-team' ), $args );
}

function sa_vc_team_add_meta_box() {

		add_meta_box(
			'myplugin_sectionid',
			__( 'Designation and Social Links', 'svc_team' ),
			'sa_vc_team_meta_box_callback',
			'svc-team'
		);
}
add_action( 'add_meta_boxes', 'sa_vc_team_add_meta_box' );

function sa_vc_team_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'svc_testimonial_save_meta_box_data', 'svc_testimonial_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$des_value = get_post_meta( $post->ID, 'svc_designation', true );
	$fb_value = get_post_meta( $post->ID, 'svc_social_fb', true );
	$tw_value = get_post_meta( $post->ID, 'svc_social_twitter', true );
	$gp_value = get_post_meta( $post->ID, 'svc_social_gplus', true );
	$ld_value = get_post_meta( $post->ID, 'svc_social_linkdin', true );
	$ig_value = get_post_meta( $post->ID, 'svc_social_insta', true );
	$db_value = get_post_meta( $post->ID, 'svc_social_dribble', true );
	$pn_value = get_post_meta( $post->ID, 'svc_social_pin', true );
	$em_value = get_post_meta( $post->ID, 'svc_social_email', true );
	ob_start();?>
	<table>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Designation : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_designation_value" value="<?php echo esc_attr( $des_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Facebook : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_fb_value" value="<?php echo esc_attr( $fb_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Twitter : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_tw_value" value="<?php echo esc_attr( $tw_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Google Plus : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_gp_value" value="<?php echo esc_attr( $gp_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Linkdin : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_ld_value" value="<?php echo esc_attr( $ld_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Instagram : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_ig_value" value="<?php echo esc_attr( $ig_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Dribble : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_db_value" value="<?php echo esc_attr( $db_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Pinterest : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_pin_value" value="<?php echo esc_attr( $pn_value );?>" /></td>
		</tr>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Email : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_email_value" value="<?php echo esc_attr( $em_value );?>" /></td>
		</tr>
	</table>
	<?php
	$m = ob_get_clean();
	echo $m;
}

function sa_vc_team_save_meta_box_data( $post_id ) {

	// Check if our nonce is set.
	if ( ! isset( $_POST['svc_testimonial_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['svc_testimonial_meta_box_nonce'], 'svc_testimonial_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Sanitize user input.
	$designation_data = sanitize_text_field( $_POST['svc_designation_value'] );
	$fb_data = sanitize_text_field( $_POST['svc_fb_value'] );
	$tw_data = sanitize_text_field( $_POST['svc_tw_value'] );
	$gp_data = sanitize_text_field( $_POST['svc_gp_value'] );
	$ld_data = sanitize_text_field( $_POST['svc_ld_value'] );
	$ig_data = sanitize_text_field( $_POST['svc_ig_value'] );
	$db_data = sanitize_text_field( $_POST['svc_db_value'] );
	$pin_data = sanitize_text_field( $_POST['svc_pin_value'] );
	$email_data = sanitize_text_field( $_POST['svc_email_value'] );

	//Update the meta field in the database.
	update_post_meta( $post_id, 'svc_designation', $designation_data );
	update_post_meta( $post_id, 'svc_social_fb', $fb_data );
	update_post_meta( $post_id, 'svc_social_twitter', $tw_data );
	update_post_meta( $post_id, 'svc_social_gplus', $gp_data );
	update_post_meta( $post_id, 'svc_social_linkdin', $ld_data );
	update_post_meta( $post_id, 'svc_social_insta', $ig_data );
	update_post_meta( $post_id, 'svc_social_dribble', $db_data );
	update_post_meta( $post_id, 'svc_social_pin', $pin_data );
	update_post_meta( $post_id, 'svc_social_email', $email_data );
}
add_action( 'save_post', 'sa_vc_team_save_meta_box_data' );

function sa_vc_team_excerpt($excerpt,$limit) {
	$excerpt = strip_tags($excerpt);
	$excerpt = explode(' ', $excerpt, $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

function sa_vc_team_hex2rgba($hex,$opacity) {
   $hex = str_replace("#", "", $hex);
	if(strlen($hex) == 3){
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	}else{
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
	$rgba = array($r, $g, $b, $opacity);
	return 'rgba('.implode(",", $rgba).')';
}


//post grid list layouts

function sa_vc_post_layout_excerpt($excerpt,$limit) {
	$excerpt = strip_tags($excerpt);
	$excerpt = explode(' ', $excerpt, $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

add_action('wp_ajax_sa_vc_layout_post','sa_vc_layout_post');
add_action('wp_ajax_nopriv_sa_vc_layout_post','sa_vc_layout_post');
function sa_vc_layout_post(){
	extract($_POST);
	echo do_shortcode('[sa_vc_post_layout query_loop="'.$query_loop.'" grid_link_target="'.$grid_link_target.'" grid_layout_mode="'.$grid_layout_mode.'" grid_columns_count_for_desktop="'.$grid_columns_count_for_desktop.'" grid_columns_count_for_tablet="'.$grid_columns_count_for_tablet.'" grid_columns_count_for_mobile="'.$grid_columns_count_for_mobile.'" grid_thumb_size="'.$grid_thumb_size.'" svc_excerpt_length="'.$svc_excerpt_length.'" skin_type="'.$skin_type.'" title="'.$title.'" effect="'.$effect.'" read_more="'.$read_more.'" svc_class="'.$svc_class.'" dexcerpt="'.$dexcerpt.'" dfeatured="'.$dfeatured.'" dpost_popup="'.$dpost_popup.'" dcategory="'.$dcategory.'" dmeta_data="'.$dmeta_data.'" dimg_popup="'.$dimg_popup.'" dsocial="'.$dsocial.'" pbgcolor="'.$pbgcolor.'" pbghcolor="'.$pbghcolor.'" tcolor="'.$tcolor.'" thcolor="'.$thcolor.'" load_more_color="'.$load_more_color.'" popup_bgcolor="'.$popup_bgcolor.'" popup_line_color="'.$popup_line_color.'" popup_max_width="'.$popup_max_width.'" paged="'.$paged.'" svc_grid_id="'.$svc_grid_id.'" ajax="1"]');
	die();
}

add_action('wp_ajax_sa_vc_post_layout_carousel','sa_vc_post_layout_carousel');
add_action('wp_ajax_nopriv_sa_vc_post_layout_carousel','sa_vc_post_layout_carousel');
function sa_vc_post_layout_carousel(){
	extract($_POST);
	echo do_shortcode('[sa_vc_post_layout svc_type="'.$svc_type.'" query_loop="'.$query_loop.'" grid_link_target="'.$grid_link_target.'" grid_layout_mode="'.$grid_layout_mode.'" grid_thumb_size="'.$grid_thumb_size.'" svc_excerpt_length="'.$svc_excerpt_length.'" skin_type="'.$skin_type.'" title="'.$title.'" read_more="'.$read_more.'" svc_class="'.$svc_class.'" dexcerpt="'.$dexcerpt.'" dfeatured="'.$dfeatured.'" dpost_popup="'.$dpost_popup.'" dcategory="'.$dcategory.'" dmeta_data="'.$dmeta_data.'" dimg_popup="'.$dimg_popup.'" dsocial="'.$dsocial.'" pbgcolor="'.$pbgcolor.'" pbghcolor="'.$pbghcolor.'" tcolor="'.$tcolor.'" thcolor="'.$thcolor.'" paged="'.$paged.'" svc_grid_id="'.$svc_grid_id.'" ajax="1"]');
	die();
}

add_action('wp_ajax_sa_vc_inline_post_popup','sa_vc_inline_post_popup');
add_action('wp_ajax_nopriv_sa_vc_inline_post_popup','sa_vc_inline_post_popup');
function sa_vc_inline_post_popup(){
	extract($_GET);
	$post = get_post($pid);
	$post_type = $post->post_type;
    $content = apply_filters('the_content', $post->post_content);
	?>
	<div class="svc-magnific-popup-countainer svc-magnific-popup-countainer-<?php echo $pid;?>">
    <style type="text/css">
	<?php if($bgcolor != ''){?>
	.svc-magnific-popup-countainer{background-color:#<?php echo $bgcolor;?> !important;}
	<?php }
	if($line_color != ''){?>
	.svc-magnific-popup-countainer{border-bottom-color:#<?php echo $line_color;?> !important;}
	<?php }
	if($max_width != ''){?>
	.svc-magnific-popup-countainer{max-width:<?php echo $max_width;?>px !important;}
	<?php }?>
	.svc-popup-img-div{ text-align:center; line-height:0;}
	.svc-content-countainer{padding:2% 4% 3%; width:auto;}
	.svc-magnific-popup-countainer .svc_post_cat{ margin-bottom:10px;}
	.svc-magnific-popup-countainer .svc_social_share > ul li{margin-right: 0px !important;padding: 3px 6px;float:left;margin-bottom:0px; list-style:none;}
	.svc-magnific-popup-countainer .svc_social_share{display: inline-block;float: none;position: relative;margin-top:10px;}
	.svc-magnific-popup-countainer .svc_social_share ul{ padding:0px !important; text-indent:0 !important;}
	.svc-magnific-popup-countainer .svc_social_share > ul li a {font-size: 13px;color:#fff !important;}
	.svc-magnific-popup-countainer .svc_social_share > ul li:first-child{background:#6CDFEA;}
	.svc-magnific-popup-countainer .svc_social_share > ul li:nth-child(2){background:#3B5998;padding:3px 8.5px !important;}
	.svc-magnific-popup-countainer .svc_social_share > ul li:nth-child(3){background:#E34429;}
	</style>
    <?php if($fi != 'yes'){?>
	<div class="svc-popup-img-div"><?php echo get_the_post_thumbnail( $pid, 'full'); ?></div>
    <?php }?>
	<div class="svc-content-countainer">
	<h1><?php echo get_the_title($pid);?></h1>
	<?php 
	$tax_co= 0;
	$post_taxonomies = get_object_taxonomies( $post_type );
	for($i = 0;$i < count($post_taxonomies); $i++){
		if($post_taxonomies[$i] == 'post_format'){
			unset($post_taxonomies[$i]);
		}
	}
	foreach ($post_taxonomies as $taxonomy) {
		if($taxonomy != 'post_tag'){
			$terms = get_the_terms( $pid, $taxonomy );
			if ($tax_co==0){?>
			<div class="svc_post_cat">
				<i class="fa fa-tags"></i>
			<?php }
			if ( !empty( $terms ) ) {
			  foreach ( $terms as $term ) {
			  if($tax_co>0){echo ', ';}
			  ?>
				 <a href="<?php echo get_term_link( $term->slug, $taxonomy );?>"><?php echo $term->name;?></a>
			<?php
			$tax_co++;
			  }
			}
		}
	}
	if($tax_co!= 0 ){?>
	</div>
	<?php }
	echo $content;?>
	<div class="svc_social_share">
		<ul>
		  <li><a href="https://twitter.com/intent/tweet?text=&url=<?php echo $post->link?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
		  <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post->link?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
		  <li><a href="https://plusone.google.com/share?url=<?php echo $post->link?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
		</ul>
	</div>
	</div>
	</div>
	<?php
	die();
}

function sa_vc_kriesi_pagination($pages = '',$svc_grid_id, $range = 2){  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='svc_pagination svc_pagination_".$svc_grid_id."'>";
         //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             //if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                 echo ($paged == $i)? "<a href='".get_pagenum_link($i)."' class='current' page='".$i."'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' page='".$i."'>".$i."</a>";
             //}
         }

         //if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."' page='".($paged + 1)."'>&rsaquo;</a>";  
         //if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."' page='".($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

// woo grid layout

function sa_vc_woo_quick_view_action_template() {

	// Image
	//add_action( 'sa_vc_woo_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	//add_action( 'sa_vc_woo_product_summary', 'woocommerce_show_product_images', 20 );

	// Summary
	add_action( 'sa_vc_woo_product_summary', 'woocommerce_template_single_title', 5 );
	add_action( 'sa_vc_woo_product_summary', 'woocommerce_template_single_rating', 10 );
	add_action( 'sa_vc_woo_product_summary', 'woocommerce_template_single_price', 15 );
	add_action( 'sa_vc_woo_product_summary', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'sa_vc_woo_product_summary', 'woocommerce_template_single_add_to_cart', 25 );
	add_action( 'sa_vc_woo_product_summary', 'woocommerce_template_single_meta', 30 );
}

function sa_vc_woo_layout_excerpt($excerpt,$limit) {
	$excerpt = strip_tags($excerpt);
	$excerpt = explode(' ', $excerpt, $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

add_action('wp_ajax_sa_vc_layout_woo','sa_vc_layout_woo');
add_action('wp_ajax_nopriv_sa_vc_layout_woo','sa_vc_layout_woo');
function sa_vc_layout_woo(){
	extract($_POST);
	echo do_shortcode('[sa_vc_woo_layout query_loop="'.$query_loop.'" grid_link_target="'.$grid_link_target.'" grid_layout_mode="'.$grid_layout_mode.'" grid_columns_count_for_desktop="'.$grid_columns_count_for_desktop.'" grid_columns_count_for_tablet="'.$grid_columns_count_for_tablet.'" grid_columns_count_for_mobile="'.$grid_columns_count_for_mobile.'" grid_thumb_size="'.$grid_thumb_size.'" svc_excerpt_length="'.$svc_excerpt_length.'" skin_type="'.$skin_type.'" title="'.$title.'" effect="'.$effect.'" read_more="'.$read_more.'" svc_class="'.$svc_class.'" dexcerpt="'.$dexcerpt.'" dcategory="'.$dcategory.'" dmeta_data="'.$dmeta_data.'" dsocial="'.$dsocial.'" pbgcolor="'.$pbgcolor.'" pbghcolor="'.$pbghcolor.'" tcolor="'.$tcolor.'" thcolor="'.$thcolor.'" load_more_color="'.$load_more_color.'" popup_bgcolor="'.$popup_bgcolor.'" popup_line_color="'.$popup_line_color.'" popup_max_width="'.$popup_max_width.'" multi_img="'.$multi_img.'" paged="'.$paged.'" svc_grid_id="'.$svc_grid_id.'" ajax="1"]');
	die();
}

add_action('wp_ajax_sa_vc_woo_layout_carousel','sa_vc_woo_layout_carousel');
add_action('wp_ajax_nopriv_sa_vc_woo_layout_carousel','sa_vc_woo_layout_carousel');
function sa_vc_woo_layout_carousel(){
	extract($_POST);
	echo do_shortcode('[sa_vc_woo_layout svc_type="'.$svc_type.'" query_loop="'.$query_loop.'" grid_link_target="'.$grid_link_target.'" grid_layout_mode="'.$grid_layout_mode.'" grid_thumb_size="'.$grid_thumb_size.'" svc_excerpt_length="'.$svc_excerpt_length.'" skin_type="'.$skin_type.'" title="'.$title.'" read_more="'.$read_more.'" svc_class="'.$svc_class.'" dexcerpt="'.$dexcerpt.'" dcategory="'.$dcategory.'" dmeta_data="'.$dmeta_data.'" dsocial="'.$dsocial.'" pbgcolor="'.$pbgcolor.'" pbghcolor="'.$pbghcolor.'" tcolor="'.$tcolor.'" thcolor="'.$thcolor.'" multi_img="'.$multi_img.'" paged="'.$paged.'" svc_grid_id="'.$svc_grid_id.'" ajax="1"]');
	die();
}

add_action('wp_ajax_sa_vc_inline_woo_popup','sa_vc_inline_woo_popup');
add_action('wp_ajax_nopriv_sa_vc_inline_woo_popup','sa_vc_inline_woo_popup');
function sa_vc_inline_woo_popup(){
	extract($_GET);
	$post = get_post($pid);
	$post_type = $post->post_type;
    $content = apply_filters('the_content', $post->post_content);
	?>
	<div class="svc-magnific-popup-countainer svc-magnific-popup-countainer-<?php echo $pid;?>">
	<script src="<?php echo plugins_url('../assets/js/add-to-cart-variation.js', __FILE__);?>"></script>
    <style type="text/css">
	.svc-magnific-popup-countainer p{margin-bottom:10px;}
	<?php if($bgcolor != ''){?>
	.svc-magnific-popup-countainer{background-color:#<?php echo $bgcolor;?> !important;}
	<?php }
	if($line_color != ''){?>
	.svc-magnific-popup-countainer{border-bottom-color:#<?php echo $line_color;?> !important;}
	<?php }
	if($max_width != ''){?>
	.svc-magnific-popup-countainer{max-width:<?php echo $max_width;?>px !important;}
	<?php }?>
	.svc-popup-img-div{ text-align:center; line-height:0;}
	.svc-content-countainer{padding:2% 4% 3%; width:auto;}
	.svc-magnific-popup-countainer .svc_post_cat{ margin-bottom:10px;}
	.svc-magnific-popup-countainer .svc_social_share > ul li{margin-right: 0px !important;padding: 3px 6px;float:left;margin-bottom:0px; list-style:none;}
	@media screen and (min-width:769px){
		.svc-magnific-popup-countainer .svc_social_share{display: inline-block;float: none;position: absolute;margin-top:0px;left:100%; top:0px;}
	}
	@media screen and (max-width:768px){
		.svc-magnific-popup-countainer .svc_social_share{display: inline-block;float: none;position: absolute;margin-top:0px;right:0; bottom:0px;}
	}
	.svc-magnific-popup-countainer .svc_social_share ul{ padding:0px !important; text-indent:0 !important;margin-top: 0;}
	.svc-magnific-popup-countainer .svc_social_share > ul li a {font-size: 13px;color:#fff !important;}
	.svc-magnific-popup-countainer .svc_social_share > ul li:first-child{background:#6CDFEA;}
	.svc-magnific-popup-countainer .svc_social_share > ul li:nth-child(2){background:#3B5998;padding:3px 8.5px !important;}
	.svc-magnific-popup-countainer .svc_social_share > ul li:nth-child(3){background:#E34429;}
	.svc_owl_slider img,.svc_owl_slider_thum img{width:100%;}
	.owl-wrapper .owl-item.synced::before {
		border-bottom: 3px solid #869791;
		content: "";
		margin-top: -3px;
		position: absolute;
		top: 0;
		left:0;
		width: 100%;
	}
	.owl-wrapper .owl-item.synced::after {
		border-bottom: 8px solid #869791;
		border-left: 8px solid rgba(0, 0, 0, 0);
		border-right: 8px solid rgba(0, 0, 0, 0);
		content: "";
		height: 0;
		left: 50%;
		margin-left: -8px;
		position: absolute;
		top: -10px;
		width: 0;
	}
	#sync2 .owl-item {
		line-height: 0;
		margin-top: 15px;
	}
	</style>
	<div class="woo-popupp">
	<div class="sa-col-md-5 sa-col-12 sa-nopadding">
		<div style="width:100%;">
		<?php if($fi != 'yes'){?>
		<div class="svc-popup-img-div">
			<div id="sync1" class="owl-carousel">
				<div class="svc_owl_slider">
				<?php echo get_the_post_thumbnail( $pid, 'full');?>
				</div>
				<?php 
				$imgs = explode(',',get_post_meta($pid,'_product_image_gallery',true));
				if(count($imgs)>0 && !empty($imgs[0])){
					foreach($imgs as $img){
						$image_attributes = wp_get_attachment_image_src($img,'full');
						if( $image_attributes ) {?>
						<div class="svc_owl_slider"><img src="<?php echo $image_attributes[0]; ?>"></div>
						<?php }
					}
				}?>
			</div>
			
			<?php if(count($imgs)>0 && !empty($imgs[0])){?>
			<div id="sync2" class="owl-carousel">
				<div class="svc_owl_slider_thum">
				<?php echo get_the_post_thumbnail( $pid, 'thumbnail');?>
				</div>
				<?php 
				if(count($imgs)>0){
					foreach($imgs as $img){
						$image_attributes = wp_get_attachment_image_src($img,'thumbnail');
						if( $image_attributes ) {?> 
						<div class="svc_owl_slider_thum"><img src="<?php echo $image_attributes[0]; ?>"></div>
						<?php }
					}
				}?>
			</div>
			<?php }?>
		</div>
		<?php }?>
		</div>
    </div>
    <div class="sa-col-md-7 sa-col-12">
		<div class="svc-content-countainer">
		<?php 
		wp( 'p=' . $pid . '&post_type=product' );
		while ( have_posts() ) : the_post(); ?>
		<div class="product">
		<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="summary entry-summary woocommerce">
			<?php do_action( 'sa_vc_woo_product_summary' ); ?>
		</div>
		</div>
		</div>
		<?php endwhile;?>
		</div>
	</div>
	</div>
	<div class="svc_social_share">
		<ul>
		  <li><a href="https://twitter.com/intent/tweet?text=&url=<?php echo get_the_permalink($pid);?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
		  <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink($pid);?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
		  <li><a href="https://plusone.google.com/share?url=<?php echo get_the_permalink($pid);?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
		</ul>
	</div>
	<div style="clear:both"></div>
	</div>
	<script>
    jQuery(function($) {
	var j = 0;
	var variation_a = setInterval(function(){
		$( '.variations_form' ).wc_variation_form();
		$( '.variations_form .variations select' ).change();
		//console.log(wc_add_to_cart_variation_params);
		/*if ( typeof wc_add_to_cart_variation_params !== 'undefined' ) {
		$( '.variations_form' ).each( function() {
			$( this ).wc_variation_form().find('.variations select:eq(0)').change();
		});
		}*/
		if(j == 2){clearInterval(variation_a);}
		j++;
	},1000);
      var sync1 = $("#sync1");
      var sync2 = $("#sync2");
     
      sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: true,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
        navigationText: [
			"<i class='fa fa-chevron-left icon-white'></i>",
			"<i class='fa fa-chevron-right icon-white'></i>"
		],
      });
     
      sync2.owlCarousel({
        items : 4,
        itemsDesktop      : [1199,4],
        itemsDesktopSmall     : [979,4],
        itemsTablet       : [768,4],
        itemsMobile       : [479,4],
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
      });
     
      function syncPosition(el){
        var current = this.currentItem;
        $("#sync2")
          .find(".owl-item")
          .removeClass("synced")
          .eq(current)
          .addClass("synced")
        if($("#sync2").data("owlCarousel") !== undefined){
          center(current)
        }
      }
     
      $("#sync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
      });
     
      function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }
     
        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2)
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            sync2.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
          sync2.trigger("owl.goTo", num-1)
        }
        
      }
     

    });
	</script>
	<?php
	die();
}
function sa_vc_woo_kriesi_paginationn($pages = '',$svc_grid_id, $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='svc_pagination svc_pagination_".$svc_grid_id."'>";
         //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             //if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                 echo ($paged == $i)? "<a href='".get_pagenum_link($i)."' class='current' page='".$i."'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' page='".$i."'>".$i."</a>";
             //}
         }

         //if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."' page='".($paged + 1)."'>&rsaquo;</a>";  
         //if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."' page='".($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}




//testimonial start
function sa_vc_testimonial_register_post_type(){

	// Set UI labels for Custom Post Type
		$labels = array(
			'name'                => _x( 'Testimonials', 'Post Type General Name', 'twentythirteen' ),
			'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'twentythirteen' ),
			'menu_name'           => __( 'Testimonials', 'twentythirteen' ),
			'parent_item_colon'   => __( 'Parent Testimonial', 'twentythirteen' ),
			'all_items'           => __( 'All Testimonials', 'twentythirteen' ),
			'view_item'           => __( 'View Testimonial', 'twentythirteen' ),
			'add_new_item'        => __( 'Add New Testimonial', 'twentythirteen' ),
			'add_new'             => __( 'Add New', 'twentythirteen' ),
			'edit_item'           => __( 'Edit Testimonial', 'twentythirteen' ),
			'update_item'         => __( 'Update Testimonial', 'twentythirteen' ),
			'search_items'        => __( 'Search Testimonial', 'twentythirteen' ),
			'not_found'           => __( 'Not Found', 'twentythirteen' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
		);
		
	// Set other options for Custom Post Type
		
		$args = array(
			'label'               => __( 'testimonials', 'twentythirteen' ),
			'description'         => __( 'Testimonial reviews', 'twentythirteen' ),
			'labels'              => $labels,
			// Features this CPT supports in Post Editor
			'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			// You can associate this CPT with a taxonomy or custom taxonomy. 
			'taxonomies'          => array( 'testimonial-category' ),
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/	
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		
		// Registering your Custom Post Type
		register_post_type( 'svc-testimonial', $args );
}

function sa_vc_testimonial_create_testimonial_taxonomies(){
	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Categories' ),
		'all_items'         => __( 'All Categories' ),
		'parent_item'       => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item'         => __( 'Edit Category' ),
		'update_item'       => __( 'Update Category' ),
		'add_new_item'      => __( 'Add New Category' ),
		'new_item_name'     => __( 'New Category Name' ),
		'menu_name'         => __( 'Category' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'testimonial-category' ),
	);

	register_taxonomy( 'testimonial-category', array( 'svc-testimonial' ), $args );
}

function sa_vc_testimonial_add_meta_box() {

		add_meta_box(
			'myplugin_sectionid',
			__( 'Designation', 'svc_team' ),
			'sa_vc_testimonial_meta_box_callback',
			'svc-testimonial'
		);
}
add_action( 'add_meta_boxes', 'sa_vc_testimonial_add_meta_box' );

function sa_vc_testimonial_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'svc_testimonial_save_meta_box_data', 'svc_testimonial_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	$des_value = get_post_meta( $post->ID, 'svc_designation', true );
	ob_start();?>
	<table>
		<tr>
			<td><label for="myplugin_new_field"><?php _e( 'Designation : ', 'svc_team' );?></label></td>
			<td><input type="text" name="svc_designation_value" value="<?php echo esc_attr( $des_value );?>" /></td>
		</tr>
	</table>
	<?php
	$m = ob_get_clean();
	echo $m;
}

function sa_vc_testimonial_save_meta_box_data( $post_id ) {

	// Check if our nonce is set.
	if ( ! isset( $_POST['svc_testimonial_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['svc_testimonial_meta_box_nonce'], 'svc_testimonial_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Sanitize user input.
	$designation_data = sanitize_text_field( $_POST['svc_designation_value'] );

	//Update the meta field in the database.
	update_post_meta( $post_id, 'svc_designation', $designation_data );
}
add_action( 'save_post', 'sa_vc_testimonial_save_meta_box_data' );

function sa_vc_testimonial_excerpt($excerpt,$limit) {
	$excerpt = strip_tags($excerpt);
	$excerpt = explode(' ', $excerpt, $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

function sa_vc_testimonial_hex2rgba($hex,$opacity) {
   $hex = str_replace("#", "", $hex);
	if(strlen($hex) == 3){
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	}else{
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
	$rgba = array($r, $g, $b, $opacity);
	return 'rgba('.implode(",", $rgba).')';
}
?>