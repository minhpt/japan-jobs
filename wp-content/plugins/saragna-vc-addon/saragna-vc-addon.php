<?php
/**
 * Plugin Name: Saragna : Visual Composer Addons (shared on wplocker.com)
 * Description: Saragna : Visual Composer Addons
 * Version: 1.0
 * Author: Hitesh Khunt
 * Author URI: http://www.saragna.com/Hitesh-Khunt
 * Plugin URI: http://plugin.saragna.com/vc-addons
 * License: GPLv2 or later
 * Text Domain: sa_vc
 *
 */

$savcgrVersion = "1.0";

$currentFile = __FILE__;

$currentFolder = dirname($currentFile);
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

add_action('admin_init','Sa_Init_Addon');
require_once( 'inc/add-param.php' );
require_once( 'inc/noerror.php' );
require_once( 'inc/all_function.php' );
require_once( 'addons/social-stream/admin.php' );
require_once( 'addons/animated-text/admin.php' );
require_once( 'addons/post-acco/admin.php' );
require_once( 'addons/carousel/post-grid.php' );
require_once( 'addons/price-table/price-table.php' );
require_once( 'addons/team/admin.php' );
//require_once( 'addons/testimonial/admin.php' );
require_once( 'addons/post-grid/post-grid.php' );
require_once( 'addons/liquid-tab/liquid-tab.php' );
require_once( 'addons/popup/admin.php' );
require_once( 'addons/promo-box/admin.php' );


if ( is_plugin_active('woocommerce/woocommerce.php') ) {
	require_once( 'addons/post-woo-grid/post-woo-grid.php' );
}

if(is_admin()){
	wp_enqueue_style( 'sa-vc-social-admin-css', plugins_url( ltrim( 'assets/css/admin.css', '/' ), __FILE__ ), array(), '' );
}


function Sa_Init_Addon() {
	$required_vc 	= '3.9.9';
	if (defined('WPB_VC_VERSION')){
		if (version_compare($required_vc, WPB_VC_VERSION, '>')) {
			add_action('admin_notices', 'sa_vc_social_Admin_Notice_Version');
		}
	}else{
		add_action('admin_notices', 'sa_vc_social_Admin_Notice_Activation');
	}
}
function sa_vc_social_Admin_Notice_Version() {
		echo '<div class="updated"><p>The <strong>Saragna : Visual Composer Addons</strong> add-on requires <strong>Visual Composer</strong> version 4.0.0 or greater.</p></div>';	
	}
function sa_vc_social_Admin_Notice_Activation() {
	echo '<div class="updated"><p>The <strong>Saragna : Visual Composer Addons</strong> add-on requires the <strong>Visual Composer</strong> Plugin installed and activated.</p></div>';
}
?>