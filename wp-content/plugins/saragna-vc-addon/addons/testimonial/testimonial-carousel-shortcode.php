<?php
function sa_vc_testimonial_carousel_shortcode($attr,$content=null){
	extract(shortcode_atts( array(
		'title' => '',
		'svc_type' => 'carousel',
		'query_loop' => 'size:10|order_by:date|order:DESC|post_type:svc-testimonial',
		'skin_type_for_post' => 's1',
		'skin_type_for_carousel' => 's1',
		'car_display_item' => '2',
		'car_pagination' => '',
		'car_pagination_num' => '',
		'car_navigation' => '',
		'car_autoplay' => '',
		'car_autoplay_time' => '5',
		'synced' => '',
		'synced_display' => '10',
		'car_transition' => '',
		'grid_columns_count_for_desktop' => 'sa-col-md-6',
		'grid_columns_count_for_tablet' => 'sa-col-sm-6',
		'grid_columns_count_for_mobile' => 'sa-col-xs-12',
		'grid_thumb_size' => 'full',
		'svc_excerpt_length' => '20',
		'svc_class' => '',
		'hide_designation' => '',
		'hide_description' => '',
		'hide_social' => '',
		'pbgcolor' => '',
		'phbgcolor' => '',
		'tcolor' => '',
		'designation_text_color' => '',
		'content_text_color' => '',
		'nav_color'=> '',
		'border_color' => ''
	), $attr));
	wp_enqueue_script('sa-vc-imagesloaded-js');
	wp_enqueue_script('sa-vc-carousel-js');
	
	wp_register_style( 'sa-vc-testimonial-css', plugins_url('css/css.css', __FILE__));
	wp_enqueue_style('sa-vc-testimonial-css');
	
	
	$var = get_defined_vars();
	$loop=$query_loop;
	$posts = array();
	if(empty($loop)) return;

	//$paged = 1;
	$query=$query_loop;
	$query=explode('|',$query);
	
	$query_posts_per_page=10;
	$query_post_type='post';
	$query_meta_key='';
	$query_orderby='date';
	$query_order='ASC';
	
	$query_by_id='';
	$query_by_id_not_in='';
	$query_by_id_in='';
	
	$query_categories='';
	$query_cat_not_in='';
	$query_cat_in='';

	$query_tags='';
	$query_tags_in='';
	$query_tags_not_in='';
	
	$query_author='';
	$query_author_in='';
	$query_author_not_in='';
	
	$query_tax_query='';
	
	foreach($query as $query_part)
	{
		$q_part=explode(':',$query_part);
		switch($q_part[0])
		{
			case 'post_type':
				$query_post_type=explode(',',$q_part[1]);
			break;
			
			case 'size':
				$query_posts_per_page=($q_part[1]=='All' ? -1:$q_part[1]);
			break;
			
			case 'order_by':
				
				$query_meta_key='';
				$query_orderby='';
				
				$public_orders_array=array('ID','date','author','title','modified','rand','comment_count','menu_order');
				if(in_array($q_part[1],$public_orders_array))
				{
					$query_orderby=$q_part[1];
				}else
				{
					$query_meta_key=$q_part[1];
					$query_orderby='meta_value_num';
				}
				
			break;
			
			case 'order':
				$query_order=$q_part[1];
			break;
			
			case 'by_id':
				$query_by_id=explode(',',$q_part[1]);
				$query_by_id_not_in=array();
				$query_by_id_in=array();
				foreach($query_by_id as $ids)
				{
					if($ids<0)
					{
						$query_by_id_not_in[]=$ids;
					}else{
						$query_by_id_in[]=$ids;
					}
				}
			break;
			
			case 'categories':
				$query_categories=explode(',',$q_part[1]);
				$query_cat_not_in=array();
				$query_cat_in=array();
				foreach($query_categories as $cat)
				{
					if($cat<0)
					{
						$query_cat_not_in[]=$cat;
					}else
					{
						$query_cat_in[]=$cat;
					}
				}
			break;
			
			case 'tags':
				$query_tags=explode(',',$q_part[1]);
				$query_tags_not_in=array();
				$query_tags_in=array();
				foreach($query_tags as $tags)
				{
					if($tags<0)
					{
						$query_tags_not_in[]=$tags;
					}else
					{
						$query_tags_in[]=$tags;
					}
				}
			break;
			
			case 'authors':
				$query_author=explode(',',$q_part[1]);
				$query_author_not_in=array();
				$query_author_in=array();
				foreach($query_author as $author)
				{
					if($tags<0)
					{
						$query_author_not_in[]=$author;
					}else
					{
						$query_author_in[]=$author;
					}
				}
				
			break;

			case 'tax_query':
				$all_tax=get_object_taxonomies( $query_post_type );

				$tax_query=array();
				$query_tax_query=array('relation' => 'AND');
				foreach ( $all_tax as $tax ) {
					$values=$tax;
					$query_taxs_in=array();
					$query_taxs_not_in=array();
					
					$query_taxs=explode(',',$q_part[1]);
					foreach($query_taxs as $taxs)
					{
						if($taxs<0)
						{
							$query_taxs_not_in[]=$taxs;
						}else
						{
							$query_taxs_in[]=$taxs;
						}
					}

					if(count($query_taxs_not_in)>0)
					{
						$query_tax_query[]=array(
							'taxonomy' => $tax,
							'field'    => 'id',
							'terms'    => $query_taxs_not_in,
							'operator' => 'NOT IN',
						);
					}else if(count($query_taxs_in)>0)
					{
						$query_tax_query[]=array(
							'taxonomy' => $tax,
							'field'    => 'id',
							'terms'    => $query_taxs_in,
							'operator' => 'IN',
						);
					}
					
					break;
				}
				
			break;
		}
	}

	$query_final=array(
		'post_type' => $query_post_type,
		'post_status'=>'publish',
		'posts_per_page'=>$query_posts_per_page,
		'meta_key' => $query_meta_key,
		'orderby' => $query_orderby,
		'order' => $query_order,
		'paged'=>$paged,
		
		'post__in'=>$query_by_id_in,
		'post__not_in'=>$query_by_id_not_in,
		
		'category__in'=>$query_cat_in,
		'category__not_in'=>$query_cat_not_in,
		
		'tag__in'=>$query_tags_in,
		'tag__not_in'=>$query_tags_not_in,
		
		'author__in'=>$query_author_in,
		'author__not_in'=>$query_author_not_in,
		
		'tax_query'=>$query_tax_query
	 );

	$exclude_texo_array = explode(',',$exclude_texo);
	$my_query = new WP_Query($query_final);
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
	$svc_testimonial_id = rand(10,7000);
	ob_start();
	?>
<style type="text/css">
<?php if($skin_type_for_carousel == 's1'){
	if($border_color){?>
.testi_s1.testimonials-sections.svc_testimonial_<?php echo $svc_testimonial_id;?> .testimonial-box > div{ border-color: <?php echo $border_color;?>;}
.testi_s1.testimonials-sections.svc_testimonial_<?php echo $svc_testimonial_id;?> .testimonial-box h4{ border-top:7px solid <?php echo $border_color;?>; }
<?php }
if($tcolor){?>
.testi_s1.testimonials-sections.svc_testimonial_<?php echo $svc_testimonial_id;?> .testimonial-box h4{color:<?php echo $tcolor;?>;}
<?php }
if($designation_text_color){?>
.testi_s1.testimonials-sections.svc_testimonial_<?php echo $svc_testimonial_id;?> .testimonial-box h4 span{color:<?php echo $designation_text_color;?>;}
<?php }
if($content_text_color){?>
.testi_s1.testimonials-sections.svc_testimonial_<?php echo $svc_testimonial_id;?> .testimonial-box p{ color: <?php echo $content_text_color;?>;}
<?php }
}

if($nav_color != ''){?>
.svc_testimonial_<?php echo $svc_testimonial_id;?> .owl-theme .owl-controls .owl-buttons div,.svc_testimonial_<?php echo $svc_testimonial_id;?> .owl-theme .owl-controls .owl-page span{background: <?php echo $nav_color;?> !important;}
<?php }?>
</style>
<?php if(($svc_type == 'post_layout' && $skin_type_for_post == 's1') || ($skin_type_for_carousel == 's1' && $svc_type == 'carousel')){?>
<section class="svc_testimonial_<?php echo $svc_testimonial_id;?> <?php echo $svc_class;?> testi_s1 testimonials-sections">
<?php if($svc_type == 'carousel'){
			$grid_columns_count_for_desktop = $grid_columns_count_for_tablet = $grid_columns_count_for_mobile = 'sa-col-lg-12 sa-col-md-12 sa-col-xs-12 sa-col-sm-12';?>
		<div class="testimonial_carousel_<?php echo $svc_testimonial_id;?>">
<?php }
$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;?>
	<div class="<?php echo $grid_columns_count_for_desktop.' '.$grid_columns_count_for_tablet.' '.$grid_columns_count_for_mobile;?>">
		<div class="testimonial-box">
		    <div><?php echo wp_get_attachment_image( $img_id, $grid_thumb_size,false,array('class' => 'svc_testi_image') );?></div>
		    <?php if($hide_description != 'yes'){?>
				<p><?php echo sa_vc_testimonial_excerpt(get_the_content(),$svc_excerpt_length);?></p>
			<?php }?>
		    <h4><?php the_title();?><?php if($hide_designation != 'yes'){?>, <span><?php echo get_post_meta($post->id,'svc_designation',true);?></span><?php }?></h4>
	   </div>
	</div>
	<?php }
	wp_reset_query();
	if($svc_type == 'carousel'){?>
	</div>
	<?php }?>
</section>
<?php }


if($skin_type_for_carousel == 's2'){?>
<style type="text/css">
<?php if($border_color){?>
.testi_s2.svc_testimonial_<?php echo $svc_testimonial_id;?> .blockquote-entry{ border: 1px solid <?php echo $border_color;?>;}
.testi_s2.svc_testimonial_<?php echo $svc_testimonial_id;?> .testimonial-box h4{ border-top:7px solid <?php echo $border_color;?>; }
<?php }
if($tcolor){?>
.testi_s2.svc_testimonial_<?php echo $svc_testimonial_id;?> .blockquote-entry blockquote .footer_s2 cite{color:<?php echo $tcolor;?>;}
<?php }
if($designation_text_color){?>
.testi_s2.svc_testimonial_<?php echo $svc_testimonial_id;?> .blockquote-entry blockquote .footer_s2{color:<?php echo $designation_text_color;?>;}
<?php }
if($content_text_color){?>
.testi_s2.svc_testimonial_<?php echo $svc_testimonial_id;?> .blockquote-entry blockquote p{ color: <?php echo $content_text_color;?>;}
<?php }?>
</style>
<?php }?>
<?php if(($skin_type_for_post == 's2' && $svc_type == 'post_layout')  || ($skin_type_for_carousel == 's2' && $svc_type == 'carousel') ){?>
<section id="our-specialist" class="sa-row svc_testimonial_<?php echo $svc_testimonial_id;?> <?php echo $svc_class;?> testi_s2 inline-flex">
		<?php if($svc_type == 'carousel'){
			$grid_columns_count_for_desktop = $grid_columns_count_for_tablet = $grid_columns_count_for_mobile = 'sa-col-md-12 sa-col-sm-12 sa-col-xs-12';?>
		<div class="testimonial_carousel_<?php echo $svc_testimonial_id;?>">
<?php   }
	$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;
?>

<div class="<?php echo $grid_columns_count_for_desktop.' '.$grid_columns_count_for_tablet.' '.$grid_columns_count_for_mobile;?>">
	<div class="blockquote-entry">
		<div><?php echo wp_get_attachment_image( $img_id, $grid_thumb_size,false,array('class' => 'svc_testimonial_image') );?></div>
		<blockquote><p><?php echo sa_vc_testimonial_excerpt(get_the_content(),$svc_excerpt_length);?></p>
		<div class="footer_s2">
			<cite><?php the_title();?></cite><?php if($hide_designation != 'yes'){?>, <?php echo get_post_meta($post->id,'svc_designation',true); }?>
		</div>
		</blockquote>
	</div>
</div>
<?php $i++;
}
wp_reset_query();
	if($svc_type == 'carousel'){?>
		</div>
<?php }?>
</section>
<?php
}


if(($skin_type_for_post == 's3' && $svc_type == 'post_layout')  || ($skin_type_for_carousel == 's3' && $svc_type == 'carousel') ){?>
<section id="our-specialist" class="svc_testimonial_<?php echo $svc_testimonial_id;?> <?php echo $svc_class;?> team_s3">
	<div class="">
		<div class="sa-row">
		<?php if($svc_type == 'carousel'){
			$grid_columns_count_for_desktop = $grid_columns_count_for_tablet = $grid_columns_count_for_mobile = 'sa-col-lg-12 sa-col-md-12 sa-col-xs-12 sa-col-sm-12';?>
		<div class="team_carousel_<?php echo $svc_testimonial_id;?>">
<?php   }
	$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;
		
		//$post_thumbnail = wpb_getImageBySize(array( 'post_id' => $post->id, 'thumb_size' => $grid_thumb_size ));
		//$current_img_large = $post_thumbnail['thumbnail'];
		//$current_img_full = wp_get_attachment_image_src( $img_id[$img_counter++] , 'full' );
		//$thumb_img = wp_get_attachment_image_src($img_id,'thumbnail');
?>
			<div class="<?php echo $grid_columns_count_for_desktop.' '.$grid_columns_count_for_tablet.' '.$grid_columns_count_for_mobile;?>">
				<div class="staff">
					<div class="staff-image-container">
						<div class="staff-image-overlay">
						<?php if($hide_description != 'yes'){?>
							<p><?php echo sa_vc_team_excerpt(get_the_content(),$svc_excerpt_length);?></p>
						<?php }?>
						</div>
						<?php echo wp_get_attachment_image( $img_id, $grid_thumb_size,false,array('class' => 'svc_testimonial_image') );?>
						<?php if($hide_social != 'yes'){?>
						<div class="staff-social">
							<?php if(get_post_meta($post->id,'svc_social_fb',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_fb',true);?>"><i class="fa fa-facebook"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_twitter',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_twitter',true);?>"><i class="fa fa-twitter"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_gplus',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_gplus',true);?>"><i class="fa fa-google-plus"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_linkdin',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_linkdin',true);?>"><i class="fa fa-linkedin"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_insta',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_insta',true);?>"><i class="fa fa-instagram"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_dribble',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_dribble',true);?>"><i class="fa fa-dribbble"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_pin',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_pin',true);?>"><i class="fa fa-pinterest"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_email',true)){?>
								<a href="mailto:<?php echo get_post_meta($post->id,'svc_social_email',true);?>"><i class="fa fa-envelope-o"></i></a>
							<?php }?>
						</div>
						<?php }?>
					</div>
					<div class="staff-container">
						<h4><?php the_title();?></h4>
						<?php if($hide_designation != 'yes'){?>
						<p><?php echo get_post_meta($post->id,'svc_designation',true);?></p>
						<?php }?>
					</div>
				</div>
			</div>
<?php $i++;
}
wp_reset_query();
	if($svc_type == 'carousel'){?>
		</div>
<?php }?>
		</div>
	</div>
</section>
<?php
}

if(($skin_type_for_post == 's4' && $svc_type == 'post_layout')  || ($skin_type_for_carousel == 's4' && $svc_type == 'carousel') ){?>
<section id="our-specialist" class="svc_testimonial_<?php echo $svc_testimonial_id;?> <?php echo $svc_class;?> team_s4">
	<div class="">
		<div class="sa-row">
		<?php if($svc_type == 'carousel'){
			$grid_columns_count_for_desktop = $grid_columns_count_for_tablet = $grid_columns_count_for_mobile = 'sa-col-lg-12 sa-col-md-12 sa-col-xs-12 sa-col-sm-12';?>
		<div class="team_carousel_<?php echo $svc_testimonial_id;?>">
<?php   }
	$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;
		
		//$post_thumbnail = wpb_getImageBySize(array( 'post_id' => $post->id, 'thumb_size' => $grid_thumb_size ));
		//$current_img_large = $post_thumbnail['thumbnail'];
		//$current_img_full = wp_get_attachment_image_src( $img_id[$img_counter++] , 'full' );
		//$thumb_img = wp_get_attachment_image_src($img_id,'thumbnail');
?>
			<div class="<?php echo $grid_columns_count_for_desktop.' '.$grid_columns_count_for_tablet.' '.$grid_columns_count_for_mobile;?>">
                <div class="team-block">
                  <div class="team-photo">
                       <div class="layer-team-photo"></div>
                       <?php echo wp_get_attachment_image( $img_id, $grid_thumb_size,false,array('class' => 'svc_testimonial_image') );?>
                  </div>
                  <div class="team-text">
                      <h5><?php the_title();?></h5>
                      <?php if($hide_designation != 'yes'){?>
                      <h6><?php echo get_post_meta($post->id,'svc_designation',true);?></h6>
                      <?php }?>
                      <?php if($hide_description != 'yes'){?>
                      <p><?php echo sa_vc_team_excerpt(get_the_content(),$svc_excerpt_length);?></p>
                      <?php }?>
                      <?php if($hide_social != 'yes'){?>
                      <div class="team-share">
                          <?php if(get_post_meta($post->id,'svc_social_fb',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_fb',true);?>"><i class="fa fa-facebook"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_twitter',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_twitter',true);?>"><i class="fa fa-twitter"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_gplus',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_gplus',true);?>"><i class="fa fa-google-plus"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_linkdin',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_linkdin',true);?>"><i class="fa fa-linkedin"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_insta',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_insta',true);?>"><i class="fa fa-instagram"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_dribble',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_dribble',true);?>"><i class="fa fa-dribbble"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_pin',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_pin',true);?>"><i class="fa fa-pinterest"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_email',true)){?>
								<a href="mailto:<?php echo get_post_meta($post->id,'svc_social_email',true);?>"><i class="fa fa-envelope-o"></i></a>
							<?php }?>
                      </div>
                      <?php }?>
                  </div>
                </div>
            </div>
<?php $i++;
}
wp_reset_query();
	if($svc_type == 'carousel'){?>
		</div>
<?php }?>
		</div>
	</div>
</section>
<?php
}

if(($skin_type_for_post == 's5' && $svc_type == 'post_layout')  || ($skin_type_for_carousel == 's5' && $svc_type == 'carousel') ){?>
<section id="our-specialist" class="svc_testimonial_<?php echo $svc_testimonial_id;?> <?php echo $svc_class;?> team_s5">
	<div class="">
		<div class="sa-row">
		<?php if($svc_type == 'carousel'){
			$grid_columns_count_for_desktop = $grid_columns_count_for_tablet = $grid_columns_count_for_mobile = 'sa-col-lg-12 sa-col-md-12 sa-col-xs-12 sa-col-sm-12';?>
		<div class="team_carousel_<?php echo $svc_testimonial_id;?>">
<?php   }
	$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;
		
		//$post_thumbnail = wpb_getImageBySize(array( 'post_id' => $post->id, 'thumb_size' => $grid_thumb_size ));
		//$current_img_large = $post_thumbnail['thumbnail'];
		//$current_img_full = wp_get_attachment_image_src( $img_id[$img_counter++] , 'full' );
		//$thumb_img = wp_get_attachment_image_src($img_id,'thumbnail');
?>
		<div class="<?php echo $grid_columns_count_for_desktop.' '.$grid_columns_count_for_tablet.' '.$grid_columns_count_for_mobile;?>">
            <div class="worker">
            	<div class="worker-head">
                    <div class="worker-img">
                        <div class="worker-img-layer-1">
                            <div class="worker-img-layer-2">
                                <div class="worker-img-content">
                                    <?php echo wp_get_attachment_image( $img_id, $grid_thumb_size,false,array('class' => 'svc_testimonial_image') );?>
                                    <?php if($hide_social != 'yes'){?>
                                    <div class="worker-socials">
                                    	<div class="worker-socials-content">
											<?php if(get_post_meta($post->id,'svc_social_fb',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_fb',true);?>"><i class="fa fa-facebook"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_twitter',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_twitter',true);?>"><i class="fa fa-twitter"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_gplus',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_gplus',true);?>"><i class="fa fa-google-plus"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_linkdin',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_linkdin',true);?>"><i class="fa fa-linkedin"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_insta',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_insta',true);?>"><i class="fa fa-instagram"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_dribble',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_dribble',true);?>"><i class="fa fa-dribbble"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_pin',true)){?>
												<a href="<?php echo get_post_meta($post->id,'svc_social_pin',true);?>"><i class="fa fa-pinterest"></i></a>
											<?php }if(get_post_meta($post->id,'svc_social_email',true)){?>
												<a href="mailto:<?php echo get_post_meta($post->id,'svc_social_email',true);?>"><i class="fa fa-envelope-o"></i></a>
											<?php }?>
                                    	</div>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="worker-label">
                        <h4><?php the_title();?></h4>
                        <?php if($hide_designation != 'yes'){?>
                        <em><?php echo get_post_meta($post->id,'svc_designation',true);?></em>
                        <?php }?>
                    </div>
            	</div>
            	<?php if($hide_description != 'yes'){?>
                <p class="worker-note">
                    <?php echo sa_vc_team_excerpt(get_the_content(),$svc_excerpt_length);?>
                </p>
                <?php }?>
            </div>
        </div>
<?php $i++;
}
wp_reset_query();
	if($svc_type == 'carousel'){?>
		</div>
<?php }?>
		</div>
	</div>
</section>
<?php
}

if(($skin_type_for_post == 's6' && $svc_type == 'post_layout')  || ($skin_type_for_carousel == 's6' && $svc_type == 'carousel') ){?>
<section id="our-specialist" class="svc_testimonial_<?php echo $svc_testimonial_id;?> <?php echo $svc_class;?> team_s6">
	<div class="">
		<div class="sa-row">
		<?php if($svc_type == 'carousel'){
			$grid_columns_count_for_desktop = $grid_columns_count_for_tablet = $grid_columns_count_for_mobile = 'sa-col-lg-12 sa-col-md-12 sa-col-xs-12 sa-col-sm-12';?>
		<div class="team_carousel_<?php echo $svc_testimonial_id;?>">
<?php   }
	$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;
		
		//$post_thumbnail = wpb_getImageBySize(array( 'post_id' => $post->id, 'thumb_size' => $grid_thumb_size ));
		//$current_img_large = $post_thumbnail['thumbnail'];
		//$current_img_full = wp_get_attachment_image_src( $img_id[$img_counter++] , 'full' );
		//$thumb_img = wp_get_attachment_image_src($img_id,'thumbnail');
?>
		<div class="<?php echo $grid_columns_count_for_desktop.' '.$grid_columns_count_for_tablet.' '.$grid_columns_count_for_mobile;?>">
			<div class="worker">
                <?php echo wp_get_attachment_image( $img_id, $grid_thumb_size,false,array('class' => 'svc_testimonial_image') );?>
                <div class="worker-over">
                    <div class="clearfix">
                        <h4 class="pull-left no-margin"><?php the_title();?></h4>
                        <?php if($hide_designation != 'yes'){?>
                        <div class="pull-right worker-detail"><?php echo get_post_meta($post->id,'svc_designation',true);?></div>
                        <?php }?>
                    </div>
                    <div class="worker-over-hover">
                    <?php if($hide_description != 'yes'){?>
                        <p><?php echo sa_vc_team_excerpt(get_the_content(),$svc_excerpt_length);?></p>
                    <?php }?>
                        <div class="text-right">
                        <?php if($hide_social != 'yes'){?>
                        	<?php if(get_post_meta($post->id,'svc_social_fb',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_fb',true);?>" class="social-circle"><i class="fa fa-facebook"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_twitter',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_twitter',true);?>" class="social-circle"><i class="fa fa-twitter"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_gplus',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_gplus',true);?>" class="social-circle"><i class="fa fa-google-plus"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_linkdin',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_linkdin',true);?>" class="social-circle"><i class="fa fa-linkedin"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_insta',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_insta',true);?>" class="social-circle"><i class="fa fa-instagram"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_dribble',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_dribble',true);?>" class="social-circle"><i class="fa fa-dribbble"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_pin',true)){?>
								<a href="<?php echo get_post_meta($post->id,'svc_social_pin',true);?>" class="social-circle"><i class="fa fa-pinterest"></i></a>
							<?php }if(get_post_meta($post->id,'svc_social_email',true)){?>
								<a href="mailto:<?php echo get_post_meta($post->id,'svc_social_email',true);?>" class="social-circle"><i class="fa fa-envelope-o"></i></a>
							<?php }?>
                        <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
<?php $i++;
}
wp_reset_query();
	if($svc_type == 'carousel'){?>
		</div>
<?php }?>
		</div>
	</div>
</section>
<?php
}?>
<script>
	var wl = jQuery(window);
	jQuery(document).ready(function(){
<?php if($svc_type == 'carousel'){?>
	var sync1 = jQuery(".testimonial_carousel_<?php echo $svc_testimonial_id;?>");
	sync1.imagesLoaded(function(){
		sync1.owlCarousel({
			<?php if($car_autoplay == 'yes'){?>
			autoPlay: <?php echo $car_autoplay_time*1000;?>,
			<?php }?>
			items : <?php echo $car_display_item;?>,
			pagination:<?php if($car_pagination == 'yes'){echo 'true';}else{echo 'false';}?>,
			navigation: <?php if($car_navigation == 'yes'){echo 'false';}else{echo 'true';}?>,
			<?php if($car_pagination == 'yes' && $car_pagination_num == 'yes'){?>
			paginationNumbers:true,
			<?php }
			if($car_display_item == 1 && $car_transition != ''){?>
			transitionStyle : "<?php echo $car_transition;?>",
			<?php }
			if($car_display_item == 1){?>
			autoHeight:true,
			singleItem:true,
			<?php }
			if($synced == 'yes' && $car_display_item == 1){?>
			//afterAction : svc_syncPosition_<?php echo $svc_testimonial_id;?>,
			//responsiveRefreshRate : 200,
			<?php }?>
			 navigationText: [
				"<i class='fa fa-chevron-left icon-white'></i>",
				"<i class='fa fa-chevron-right icon-white'></i>"
			],
		});
	});
<?php }?>
});
</script>

<?php 
	$message = ob_get_clean();
	return $message;
}
?>
