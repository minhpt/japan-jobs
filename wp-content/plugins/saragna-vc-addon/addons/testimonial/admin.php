<?php
include('testimonial-shortcode.php');
include('testimonial-carousel-shortcode.php');
if(!class_exists('sa_vc_testimonial_layout'))
{
	class sa_vc_testimonial_layout
	{
		function __construct()
		{
			add_action('admin_init',array($this,'sa_vc_testimonial_layout_init'));
			add_shortcode('sa_vc_testimonial','sa_vc_testimonial_layout_shortcode');
			add_shortcode('sa_vc_testimonial_carousel','sa_vc_testimonial_carousel_shortcode');
		}
		function sa_vc_testimonial_layout_init()
		{

			if(function_exists('vc_map')){
				vc_map( array(
					"name" => __('Testimonial Grid','js_composer'),		
					"base" => 'sa_vc_testimonial',		
					"icon" => 'vc_testimonial_logo',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Testimonial grid.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'Title', 'js_composer' ),
							'param_name' => 'title',
							'holder' => 'div',
							'description' => __( 'Set Post testimonial Title.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "svc_type",
							"value" =>array(
								__("Grid/List Tab Layout", 'js_composer' )=>"post_layout",
								//__("Carousel", 'js_composer' )=>"carousel"
								),
							'std' => 'post_layout',
							"description" => __("Choose svc type.", 'js_composer' ),
						),
						array(
							'type' => 'loop',
							'heading' => __('Build Post Query','js_composer'),
							'param_name' => 'query_loop',
							'settings' => array(
								'post_type' => array('value' => 'svc-testimonial'),
								'size' => array( 'hidden' => false, 'value' => 10 ),
								'order_by' => array( 'value' => 'date' ),
								'order' => array('value' => 'DESC')
							),
							'description' => __('Create WordPress loop, to populate content from your site.','js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type_for_post",
							"value" =>array(
								__("Style1", 'js_composer' )=>"s1",
								__("Style2", 'js_composer' )=>"s2"
								),
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose skin type for grid layout.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type_for_carousel",
							"value" =>array(
								__("Style2", 'js_composer' )=>"s2",
								__("Style3", 'js_composer' )=>"s3",
								__("Style4", 'js_composer' )=>"s4",
								__("Style5", 'js_composer' )=>"s5",
								__("Style6", 'js_composer' )=>"s6",
								),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							"description" => __("Choose skin type for grid layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '2',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Synced Slider', 'js_composer' ),
							'param_name' => 'synced',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_display_item','value' => '1'),
							'description' => __( 'Set Synced Slider.see Example <a href="http://owlgraphic.com/owlcarousel/demos/sync.html" target="_black">here</a>', 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Synced Display', 'js_composer' ),
							'param_name' => 'synced_display',
							'value' => '10',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'synced','value' => 'yes'),
							'description' => __( 'Set Synces Slider Display elements.', 'js_composer' )
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Transition effect', 'js_composer' ),
							'param_name' => 'car_transition',
							'value' => array(
								__( 'None', 'js_composer' ) => '',
								__( 'fade', 'js_composer' ) => 'fade',
								__( 'backSlide', 'js_composer' ) => 'backSlide',
								__( 'goDown', 'js_composer' ) => 'goDown',
								__( 'scaleUp', 'js_composer' ) => 'scaleUp'
							),
							'dependency' => array('element' => 'car_display_item','value' => '1'),
							'description' => __( 'Add CSS3 transition style. Works only with one item on screen.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								),
							'std' => 'sa-col-md-6',
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3"
								),
							'std' => 'sa-col-sm-6',
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Thumbnail size', 'js_composer' ),
							'param_name' => 'grid_thumb_size',
							'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'Excerpt Length', 'js_composer' ),
							'param_name' => 'svc_excerpt_length',
							'value' => '20',
							'min' => 10,
							'max' => 900,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set excerpt length.default:20', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Designation', 'js_composer' ),
							'param_name' => 'hide_designation',
							'value' => 'yes',
							'description' => __( 'hide Designation.', 'js_composer' ),
							'group' => __('Display Setting', 'js_composer')
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Description', 'js_composer' ),
							'param_name' => 'hide_description',
							'value' => 'yes',
							'description' => __( 'hide Description.', 'js_composer' ),
							'group' => __('Display Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Testimonial Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Testimonial Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Testimonial Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Designation Text Color', 'js_composer' ),
							'param_name' => 'designation_text_color',
							'description' => __( 'set Designation Text color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Content Text Color', 'js_composer' ),
							'param_name' => 'content_text_color',
							'description' => __( 'set Content Text color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Carousel Navigation Color', 'js_composer' ),
							'param_name' => 'nav_color',
							'description' => __( 'set Carousel Navigation Color.', 'js_composer' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Border Color', 'js_composer' ),
							'param_name' => 'border_color',
							'description' => __( 'set Border Color.', 'js_composer' ),
							'dependency' => array('element' => 'skin_type_for_post','value' => 's1'),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );

				vc_map( array(
					"name" => __('Testimonial Carousel','js_composer'),		
					"base" => 'sa_vc_testimonial_carousel',		
					"icon" => 'vc_testimonial_caro_logo',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Testimonial Carousel.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'Title', 'js_composer' ),
							'param_name' => 'title',
							'holder' => 'div',
							'description' => __( 'Set Testimonial Title.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "svc_type",
							"value" =>array(
								//__("Grid/List Tab Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							'std' => 'carousel',
							"description" => __("Choose svc type.", 'js_composer' ),
						),
						array(
							'type' => 'loop',
							'heading' => __('Build Post Query','js_composer'),
							'param_name' => 'query_loop',
							'settings' => array(
								'post_type' => array('value' => 'svc-testimonial'),
								'size' => array( 'hidden' => false, 'value' => 10 ),
								'order_by' => array( 'value' => 'date' ),
								'order' => array('value' => 'DESC')
							),
							'description' => __('Create WordPress loop, to populate content from your site.','js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type_for_post",
							"value" =>array(
								__("Style1", 'js_composer' )=>"s1",
								__("Style2", 'js_composer' )=>"s2"
								),
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose skin type for grid layout.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type_for_carousel",
							"value" =>array(
								__("Style1", 'js_composer' )=>"s1",
								__("Style2", 'js_composer' )=>"s2",
								__("Style3", 'js_composer' )=>"s3",
								__("Style4", 'js_composer' )=>"s4",
								__("Style5", 'js_composer' )=>"s5",
								__("Style6", 'js_composer' )=>"s6",
								),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							"description" => __("Choose skin type for carousel layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '2',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Synced Slider', 'js_composer' ),
							'param_name' => 'synced',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_display_item','value' => '1'),
							'description' => __( 'Set Synced Slider.see Example <a href="http://owlgraphic.com/owlcarousel/demos/sync.html" target="_black">here</a>', 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Synced Display', 'js_composer' ),
							'param_name' => 'synced_display',
							'value' => '10',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'synced','value' => 'yes'),
							'description' => __( 'Set Synces Slider Display elements.', 'js_composer' )
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Transition effect', 'js_composer' ),
							'param_name' => 'car_transition',
							'value' => array(
								__( 'None', 'js_composer' ) => '',
								__( 'fade', 'js_composer' ) => 'fade',
								__( 'backSlide', 'js_composer' ) => 'backSlide',
								__( 'goDown', 'js_composer' ) => 'goDown',
								__( 'scaleUp', 'js_composer' ) => 'scaleUp'
							),
							'dependency' => array('element' => 'car_display_item','value' => '1'),
							'description' => __( 'Add CSS3 transition style. Works only with one item on screen.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								),
							'std' => 'sa-col-md-6',
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3"
								),
							'std' => 'sa-col-sm-6',
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'svc_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Thumbnail size', 'js_composer' ),
							'param_name' => 'grid_thumb_size',
							'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'Excerpt Length', 'js_composer' ),
							'param_name' => 'svc_excerpt_length',
							'value' => '20',
							'min' => 10,
							'max' => 900,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set excerpt length.default:20', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Designation', 'js_composer' ),
							'param_name' => 'hide_designation',
							'value' => 'yes',
							'description' => __( 'hide Designation.', 'js_composer' ),
							'group' => __('Display Setting', 'js_composer')
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Description', 'js_composer' ),
							'param_name' => 'hide_description',
							'value' => 'yes',
							'description' => __( 'hide Description.', 'js_composer' ),
							'group' => __('Display Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Testimonial Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Testimonial Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Testimonial Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Designation Text Color', 'js_composer' ),
							'param_name' => 'designation_text_color',
							'description' => __( 'set Designation Text color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Content Text Color', 'js_composer' ),
							'param_name' => 'content_text_color',
							'description' => __( 'set Content Text color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Carousel Navigation Color', 'js_composer' ),
							'param_name' => 'nav_color',
							'description' => __( 'set Carousel Navigation Color.', 'js_composer' ),
							'dependency' => array('element' => 'svc_type','value' => 'carousel'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Border Color', 'js_composer' ),
							'param_name' => 'border_color',
							'description' => __( 'set Border Color.', 'js_composer' ),
							'dependency' => array('element' => 'skin_type_for_carousel','value' => 's1'),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				
			}

		}

	}
	
	
	//instantiate the class
	$sa_vc_testimonial_layout = new sa_vc_testimonial_layout;
}
