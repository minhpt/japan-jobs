<?php
include('social-stream-shortcode.php');
if(!class_exists('sa_vc_social_layout'))
{
	class sa_vc_social_layout
	{
		function __construct()
		{
			add_action('admin_init',array($this,'sa_vc_social_layout_init'));
			add_shortcode('sa_social_fb','sa_vc_social_layout_shortcode');
			add_shortcode('sa_social_insta','sa_vc_social_layout_shortcode');
			add_shortcode('sa_social_twitter','sa_vc_social_layout_shortcode');
			add_shortcode('sa_social_gplus','sa_vc_social_layout_shortcode');
			add_shortcode('sa_social_youtube','sa_vc_social_layout_shortcode');
			add_shortcode('sa_social_tumblr','sa_vc_social_layout_shortcode');
			add_shortcode('sa_social_vimeo','sa_vc_social_layout_shortcode');
			//add_shortcode('sa_social_vk','sa_vc_social_layout_shortcode');
		}
		function sa_vc_social_layout_init()
		{

			if(function_exists('vc_map')){
				//facebook social
				vc_map( array(
					"name" => __('Facebook Feed','js_composer'),		
					"base" => 'sa_social_fb',		
					"icon" => 'vc_social_fb_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Facebook Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'User name Or Page ID', 'js_composer' ),
							'param_name' => 'fb_id',
							'holder' => 'div',
							'description' => __( 'Enter Facebook Page or User name. eg. vindiesel', 'js_composer' ),
							'group' => __('<i class="fa fa-facebook"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'fb_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-facebook"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				// instagram social
				vc_map( array(
					"name" => __('Instagram Feed','js_composer'),		
					"base" => 'sa_social_insta',		
					"icon" => 'vc_social_insta_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Instagram Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'dropdown',
							'heading' => __( 'Select Type', 'js_composer' ),
							'param_name' => 'instagram_type',
							"value" =>array(
								__("For User", 'js_composer' )=>"@",
								__("For Search Feed", 'js_composer' )=>"#"
								),
							'description' => __( 'Select Instagram User feed type.', 'js_composer' ),
							'group' => __('<i class="fa fa-instagram"></i>', 'js_composer')
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'User name', 'js_composer' ),
							'param_name' => 'instagram_id',
							'holder' => 'div',
							'description' => __( 'Enter Instagram User name. eg. shabbyapple', 'js_composer' ),
							'group' => __('<i class="fa fa-instagram"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'instagram_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-instagram"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				// twitter social
				vc_map( array(
					"name" => __('Twitter Feed','js_composer'),		
					"base" => 'sa_social_twitter',		
					"icon" => 'vc_social_twitter_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Twitter Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'dropdown',
							'heading' => __( 'Select Type', 'js_composer' ),
							'param_name' => 'twitter_type',
							"value" =>array(
								__("For User", 'js_composer' )=>"@",
								__("For Search Feed", 'js_composer' )=>"#"
								
								),
							'description' => __( 'Select Twitter User feed type.', 'js_composer' ),
							'group' => __('<i class="fa fa-twitter"></i>', 'js_composer')
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'User name', 'js_composer' ),
							'param_name' => 'twitter_id',
							'holder' => 'div',
							'description' => __( 'Enter Twitter User name. eg. flipkart', 'js_composer' ),
							'group' => __('<i class="fa fa-twitter"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'twitter_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-twitter"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				// gplus social
				vc_map( array(
					"name" => __('Gplus Feed','js_composer'),		
					"base" => 'sa_social_gplus',		
					"icon" => 'vc_social_gplus_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Gplus Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'dropdown',
							'heading' => __( 'Select Type', 'js_composer' ),
							'param_name' => 'gplus_type',
							"value" =>array(
								__("For Page ID or name", 'js_composer' )=>"#",
								__("For User Profile ID or name", 'js_composer' )=>"@"
								),
							'description' => __( 'Select Google Plus feed type.', 'js_composer' ),
							'group' => __('<i class="fa fa-google"></i>', 'js_composer')
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'User Or Page ID', 'js_composer' ),
							'param_name' => 'gplus_id',
							'holder' => 'div',
							'description' => __( 'Enter Google Plus Page or Profile id. eg. shabbyapple', 'js_composer' ),
							'group' => __('<i class="fa fa-google"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'gplus_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-google"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				// youtube social
				vc_map( array(
					"name" => __('YouTube Feed','js_composer'),		
					"base" => 'sa_social_youtube',		
					"icon" => 'vc_social_youtube_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your YouTube Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'User ID', 'js_composer' ),
							'param_name' => 'youtube_id',
							'holder' => 'div',
							'description' => __( 'Enter Youtube User id. eg. apple', 'js_composer' ),
							'group' => __('<i class="fa fa-youtube"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'youtube_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-youtube"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				// tumblr social
				vc_map( array(
					"name" => __('Tumblr Feed','js_composer'),		
					"base" => 'sa_social_tumblr',		
					"icon" => 'vc_social_tumblr_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Tumblr Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'Page ID', 'js_composer' ),
							'param_name' => 'tumblr_id',
							'holder' => 'div',
							'description' => __( 'Enter Tumblr Page id. eg. itunes', 'js_composer' ),
							'group' => __('<i class="fa fa-tumblr"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'tumblr_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-tumblr"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				// vimeo social
				vc_map( array(
					"name" => __('Vimeo Feed','js_composer'),		
					"base" => 'sa_social_vimeo',		
					"icon" => 'vc_social_vimeo_logo',		
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Vimeo Social Stream.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'User ID', 'js_composer' ),
							'param_name' => 'vimeo_id',
							'holder' => 'div',
							'description' => __( 'Enter Vimeo User id. eg. sonyprofessional', 'js_composer' ),
							'group' => __('<i class="fa fa-vimeo-square"></i>', 'js_composer')
						),
						array(
							'type' => 'num',
							'heading' => __( 'Count per page limit', 'js_composer' ),
							'param_name' => 'vimeo_num',
							'value' => '5',
							'min' => 1,
							'max' => 1000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'Set Limit for feed par page.', 'js_composer' ),
							'group' => __('<i class="fa fa-vimeo-square"></i>', 'js_composer')
						),
						array(
							"type" => "dropdown",
							"heading" => __("Feed type" , 'js_composer' ),
							"param_name" => "post_type",
							"value" =>array(
								__("Post Layout", 'js_composer' )=>"post_layout",
								__("Carousel", 'js_composer' )=>"carousel"
								),
							"description" => __("Choose Feed type.", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"template",
								__("Style2", 'js_composer' )=>"template1",
								__("Style3", 'js_composer' )=>"template2"
								),
							"description" => __("Choose skin type for Social feed layout.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Items Display', 'js_composer' ),
							'param_name' => 'car_display_item',
							'value' => '4',
							'min' => 1,
							'max' => 100,
							'suffix' => '',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'This variable allows you to set the maximum amount of items displayed at a time with the widest browser width', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination', 'js_composer' ),
							'param_name' => 'car_pagination',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Show pagination', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show pagination Numbers', 'js_composer' ),
							'param_name' => 'car_pagination_num',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'car_pagination','value' => 'yes'),
							'description' => __( 'Show numbers inside pagination buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide navigation', 'js_composer' ),
							'param_name' => 'car_navigation',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Display "next" and "prev" buttons.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'AutoPlay', 'js_composer' ),
							'param_name' => 'car_autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Slider Autoplay.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'autoPlay Time', 'js_composer' ),
							'param_name' => 'car_autoplay_time',
							'value' => '5',
							'min' => 1,
							'max' => 100,
							'suffix' => 'seconds',
							'step' => 1,
							'dependency' => array('element' => 'car_autoplay','value' => 'yes'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'max image Height', 'js_composer' ),
							'param_name' => 'max_img_height',
							'value' => '200',
							'min' => 1,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Autoplay slider speed.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Desktop Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_desktop",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-md-12",
								__("2 Columns", 'js_composer' )=>"sa-col-md-6",
								__("3 Columns", 'js_composer' )=>"sa-col-md-4",
								__("4 Columns", 'js_composer' )=>"sa-col-md-3",
								__("5 Columns", 'js_composer' )=>"sa-col-md-15"
								),
							'std' => 'sa-col-md-3',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Desktop(PC Mode) Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Tablet Columns Count" , 'js_composer' ),
							"param_name" => "grid_columns_count_for_tablet",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-sm-12",
								__("2 Columns", 'js_composer' )=>"sa-col-sm-6",
								__("3 Columns", 'js_composer' )=>"sa-col-sm-4",
								__("4 Columns", 'js_composer' )=>"sa-col-sm-3",
								__("5 Columns", 'js_composer' )=>"sa-col-sm-15"
								),
							'std' => 'sa-col-sm-4',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Tablet Columns Count", 'js_composer' ),
						),
						array(
							"type" => "dropdown",
							"class" => "",
							"heading" => __("Mobile Columns Count" ,'js_composer' ),
							"param_name" => "grid_columns_count_for_mobile",
							"value" =>array(
								__("1 Column", 'js_composer' )=>"sa-col-xs-12",
								__("2 Columns", 'js_composer' )=>"sa-col-xs-6",
								__("3 Columns", 'js_composer' )=>"sa-col-xs-4",
								__("4 Columns", 'js_composer' )=>"sa-col-xs-3",
								__("5 Columns", 'js_composer' )=>"sa-col-xs-15"
								),
							'std' => 'sa-col-xs-12',
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							"description" => __("Choose Mobile Columns Count", 'js_composer'),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'excerpt_length',
							'value' => '150',
							'min' => 0,
							'max' => 9000,
							'suffix' => '',
							'step' => 1,
							'description' => __( 'set Description length.default:150.If you set 0 no display Discription in fronted site', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Hide Media', 'js_composer' ),
							'param_name' => 'hide_media',
							'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' ),
							'description' => __( 'If you check not display Media Image in feed.', 'js_composer' )
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Show more', 'js_composer' ),
							'param_name' => 'loadmore',
							'std' => 'yes',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'dependency' => array('element' => 'post_type','value' => 'post_layout'),
							'description' => __( 'add Show more feed button.', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Show more text', 'js_composer' ),
							'param_name' => 'loadmore_text',
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'description' => __( 'add Show more button text.Default:Show More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set post background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post hover Background Color', 'js_composer' ),
							'param_name' => 'pbghcolor',
							'description' => __( 'set post hover background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Load more Loader and Text Color', 'js_composer' ),
							'param_name' => 'loder_color',
							'description' => __( 'set Load More Loader and Text color.', 'js_composer' ),
							'dependency' => array('element' => 'loadmore','value' => 'yes'),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Navigation and Pagination color', 'js_composer' ),
							'param_name' => 'car_navigation_color',
							'dependency' => array('element' => 'post_type','value' => 'carousel'),
							'description' => __( 'Set Navigation and pagination color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				
			}

		}

	}
	
	
	//instantiate the class
	$sa_vc_social_layout = new sa_vc_social_layout;
}
