<?php
function sa_vc_post_acco_layout_shortcode($attr,$content=null){
	extract(shortcode_atts( array(
		'title' => '',
		'top_description' => '',
		'query_loop' => 'size:10|order_by:date|order:DESC|post_type:post',
		'featured_hide' => '',
		'meta_hide' => '',
		'content_hide' => '',
		'all_tab_close' => '',
		'content_length' => '0',
		'tab_padding' => '0',
		'spacer' => '20',
		'grid_link_target' => 'sw',
		'top_content_position' => 'top',
		'tab_text_align' => 'left',
		'thumb_size' => '',
		'read_more' => '',
		'find_more' => '',
		'find_more_link' => '',
		'svc_class' => '',
		'pbgcolor' => '',
		'tcolor' => '',
		'btncolor' => '',
		'btn_text_color' => '',
		'content_text_color' => ''
	), $attr));
	
	wp_register_style( 'sa-vc-post-acco-css', plugins_url('css/css.css', __FILE__));
	wp_enqueue_style('sa-vc-post-acco-css');
	wp_enqueue_style('sa-vc-bootstrap-o-css');
	wp_enqueue_script('sa-vc-bootstrap-js');
	wp_enqueue_script('sa-vc-bootstrap-hover-js');
	
	$var = get_defined_vars();
	$loop=$query_loop;
	$posts = array();
	if(empty($loop)) return;

	//$paged = 1;
	$query=$query_loop;
	$query=explode('|',$query);
	
	$query_posts_per_page=10;
	$query_post_type='post';
	$query_meta_key='';
	$query_orderby='date';
	$query_order='ASC';
	
	$query_by_id='';
	$query_by_id_not_in='';
	$query_by_id_in='';
	
	$query_categories='';
	$query_cat_not_in='';
	$query_cat_in='';

	$query_tags='';
	$query_tags_in='';
	$query_tags_not_in='';
	
	$query_author='';
	$query_author_in='';
	$query_author_not_in='';
	
	$query_tax_query='';
	
	foreach($query as $query_part)
	{
		$q_part=explode(':',$query_part);
		switch($q_part[0])
		{
			case 'post_type':
				$query_post_type=explode(',',$q_part[1]);
			break;
			
			case 'size':
				$query_posts_per_page=($q_part[1]=='All' ? -1:$q_part[1]);
			break;
			
			case 'order_by':
				
				$query_meta_key='';
				$query_orderby='';
				
				$public_orders_array=array('ID','date','author','title','modified','rand','comment_count','menu_order');
				if(in_array($q_part[1],$public_orders_array))
				{
					$query_orderby=$q_part[1];
				}else
				{
					$query_meta_key=$q_part[1];
					$query_orderby='meta_value_num';
				}
				
			break;
			
			case 'order':
				$query_order=$q_part[1];
			break;
			
			case 'by_id':
				$query_by_id=explode(',',$q_part[1]);
				$query_by_id_not_in=array();
				$query_by_id_in=array();
				foreach($query_by_id as $ids)
				{
					if($ids<0)
					{
						$query_by_id_not_in[]=$ids;
					}else{
						$query_by_id_in[]=$ids;
					}
				}
			break;
			
			case 'categories':
				$query_categories=explode(',',$q_part[1]);
				$query_cat_not_in=array();
				$query_cat_in=array();
				foreach($query_categories as $cat)
				{
					if($cat<0)
					{
						$query_cat_not_in[]=$cat;
					}else
					{
						$query_cat_in[]=$cat;
					}
				}
			break;
			
			case 'tags':
				$query_tags=explode(',',$q_part[1]);
				$query_tags_not_in=array();
				$query_tags_in=array();
				foreach($query_tags as $tags)
				{
					if($tags<0)
					{
						$query_tags_not_in[]=$tags;
					}else
					{
						$query_tags_in[]=$tags;
					}
				}
			break;
			
			case 'authors':
				$query_author=explode(',',$q_part[1]);
				$query_author_not_in=array();
				$query_author_in=array();
				foreach($query_author as $author)
				{
					if($tags<0)
					{
						$query_author_not_in[]=$author;
					}else
					{
						$query_author_in[]=$author;
					}
				}
				
			break;

			case 'tax_query':
				$all_tax=get_object_taxonomies( $query_post_type );

				$tax_query=array();
				$query_tax_query=array('relation' => 'AND');
				foreach ( $all_tax as $tax ) {
					$values=$tax;
					$query_taxs_in=array();
					$query_taxs_not_in=array();
					
					$query_taxs=explode(',',$q_part[1]);
					foreach($query_taxs as $taxs)
					{
						if($taxs<0)
						{
							$query_taxs_not_in[]=$taxs;
						}else
						{
							$query_taxs_in[]=$taxs;
						}
					}

					if(count($query_taxs_not_in)>0)
					{
						$query_tax_query[]=array(
							'taxonomy' => $tax,
							'field'    => 'id',
							'terms'    => $query_taxs_not_in,
							'operator' => 'NOT IN',
						);
					}else if(count($query_taxs_in)>0)
					{
						$query_tax_query[]=array(
							'taxonomy' => $tax,
							'field'    => 'id',
							'terms'    => $query_taxs_in,
							'operator' => 'IN',
						);
					}
					
					break;
				}
				
			break;
		}
	}

	$query_final=array(
		'post_type' => $query_post_type,
		'post_status'=>'publish',
		'posts_per_page'=>$query_posts_per_page,
		'meta_key' => $query_meta_key,
		'orderby' => $query_orderby,
		'order' => $query_order,
		'paged'=>$paged,
		
		'post__in'=>$query_by_id_in,
		'post__not_in'=>$query_by_id_not_in,
		
		'category__in'=>$query_cat_in,
		'category__not_in'=>$query_cat_not_in,
		
		'tag__in'=>$query_tags_in,
		'tag__not_in'=>$query_tags_not_in,
		
		'author__in'=>$query_author_in,
		'author__not_in'=>$query_author_not_in,
		
		'tax_query'=>$query_tax_query
	 );

	$exclude_texo_array = explode(',',$exclude_texo);
	$my_query = new WP_Query($query_final);
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
	$svc_acco_id = rand(10,7000);
	ob_start();
	?>
<style type="text/css">
<?php if($tab_padding != 0){?>
.accordion-panel .panel .panel_heading<?php echo $svc_acco_id;?>{padding-top:<?php echo $tab_padding;?>px !important;padding-bottom:<?php echo $tab_padding;?>px !important;}
<?php }
if($pbgcolor != ''){?>
.accordion-panel .panel .panel-heading.panel_heading<?php echo $svc_acco_id;?>{background-color:<?php echo $pbgcolor;?>;}
.accordion-panel .panel.panel_default<?php echo $svc_acco_id;?>::before{background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, <?php echo $pbgcolor;?> 100%) repeat-x scroll 0 0 !important;}
<?php }
if($tcolor != ''){?>
.accordion-panel .panel .panel-heading.panel_heading<?php echo $svc_acco_id;?> .panel-title,.panel_heading<?php echo $svc_acco_id;?> .post_acco_meta{color:<?php echo $tcolor;?> !important;}
<?php }
if($btncolor != ''){?>
.accordion-panel .panel.panel_default<?php echo $svc_acco_id;?> .panel-body:hover .read-more{background:<?php echo $btncolor;?> !important;}
<?php }
if($btn_text_color){?>
.accordion-panel .panel.panel_default<?php echo $svc_acco_id;?> .panel-body .read-more a{color:<?php echo $btn_text_color;?> !important;}
<?php }
if($spacer != 20){?>
.accordion-panel .panel.panel_default<?php echo $svc_acco_id;?> .spacer{height: <?php echo $spacer;?>rem !important;}
<?php }
if($content_text_color != ''){?>
.accordion-panel .panel.panel_default<?php echo $svc_acco_id;?> .spacer{color:<?php echo $content_text_color;?> !important;}
<?php }?>
</style>
<article class="card accordion-card <?php echo $svc_class;?>">
<?php if(($title != '' || $top_description != '') && $top_content_position == 'top'){?>
<header>
	<?php if($title != ''){?>
	<h3 class="section-title"><?php echo $title;?></h3>
	<?php }
	if($top_description != ''){?>
	<p><?php echo $top_description;?></p>
	<?php }?>
</header>
<?php }?>
<div class="accordion-panel <?php echo $svc_class;?>">
	<div aria-multiselectable="true" role="tablist" id="accordion-<?php echo $svc_acco_id;?>" class="panel-group">
<?php 
$link_target = $grid_link_target;
	$lt = '';
	if($link_target == 'sw'){
		$lt = 'target="_self"';
	}elseif($link_target == 'nw'){
		$lt = 'target="_self"';
	}
	$i=1;
	$img_array = array();
	while ( $my_query->have_posts() ) {
		$my_query->the_post(); // Get post from query
		$post = new stdClass(); // Creating post object.
		$post->id = get_the_ID();
		$post->link = get_permalink($post->id);
		$img_id=get_post_meta( $post->id , '_thumbnail_id' ,true );
		$img_array[] = $img_id;
		
		$post_thumbnail = wpb_getImageBySize(array( 'post_id' => $post->id, 'thumb_size' => $grid_thumb_size ));
		$current_img_large = $post_thumbnail['thumbnail'];
		//$current_img_full = wp_get_attachment_image_src( $img_id[$img_counter++] , 'full' );
		$align = 'text-left';
		if($tab_text_align == 'center'){
			$align = 'text-center';
		}else if($tab_text_align == 'right'){
			$align = 'text-right';
		}
?>
		<div style="background-image: url('<?php if($featured_hide != 'yes'){echo wp_get_attachment_url($img_id);}?>');" class="panel panel-default panel_default<?php echo $svc_acco_id;?>">
			<div aria-labelledby="headingOne" role="tabpanel" class="panel-collapse collapse <?php echo ($i==1 && $all_tab_close != 'yes') ? 'in': '';?>" id="collapse<?php echo $i;?>-<?php echo $svc_acco_id;?>" style="">
				<div class="panel-body">
					<div class="read-more"><a href="<?php echo $post->link;?>" <?php echo $lt;?>><?php if($read_more == ''){ _e('Read More','svc_post_acco');}else{ _e($read_more,'svc_post_acco');}?> <i class="fa fa-arrow-right"></i></a></div>
					<div class="spacer"><?php 
					if($content_length != 0 && $content_hide != 'yes'){
					echo sa_vc_post_acco_excerpt(get_the_content(),$content_length);
					}?></div>
				</div>
			</div>
			<a aria-controls="collapse<?php echo $i;?>-<?php echo $svc_acco_id;?>" aria-expanded="true" href="#collapse<?php echo $i;?>-<?php echo $svc_acco_id;?>" data-parent="#accordion-<?php echo $svc_acco_id;?>" data-toggle="collapse" class="<?php echo $align;?>">
				<div id="heading<?php echo $i;?>-<?php echo $svc_acco_id;?>" role="tab" class="panel-heading panel_heading<?php echo $svc_acco_id;?>">
					<h4 class="panel-title"><?php echo get_the_title();?></h4>
					<?php if($meta_hide != 'yes'){?>
					<div class="post_acco_meta"><i class="fa fa-calendar"></i> <?php echo get_the_date();?></div>
					<?php }?>
				</div>
			</a>
		</div>
	<?php
	$i++;
}
?>
	</div>
</div>
<?php if(($title != '' || $top_description != '') && $top_content_position == 'bottom'){?>
<header>
	<?php if($title != ''){?>
	<h3 class="section-title"><?php echo $title;?></h3>
	<?php }
	if($top_description != ''){?>
	<p><?php echo $top_description;?></p>
	<?php }?>
</header>
<?php }
if($find_more_link != ''){?>
<footer><a href="<?php echo $find_more_link;?>" <?php echo $lt;?>><?php if($find_more == ''){ _e('Find More','svc_post_acco');}else{ _e($find_more,'svc_post_acco');}?> &nbsp; <i class="fa fa-arrow-right"></i></a></footer>
<?php }?>
</article>
	<?php
	$message = ob_get_clean();
	return $message;
}
?>
