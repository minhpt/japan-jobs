<?php
function sa_vc_promo_box_layout_shortcode($attr,$content=null){
	extract(shortcode_atts( array(
		'skin_type' => 's1',
		'image' => '',
		'icon' => '',
		'title' => '',
		'description' => '',
		'link' => '',
		'link_text' => '',
		'link_target' => 'sw',
		'thumb_size' => 'full',
		'align' => 'left',
		'title_size' => '16',
		'description_size' => '0',
		'link_size' => '14',
		'svc_class' => '',
		'bg_mask_color' => '',
		'bg_mask_hover_color' => '',
		'title_color' => '',
		'title_hover_color' => '',
		'icon_color' => '',
		'icon_hover_color' => '',
		'desc_color' => '',
		'desc_hover_color' => '',
		'link_color' => '',
		'link_hover_color' => ''
	), $attr));
	
	wp_register_style( 'sa-vc-promo-box-css', plugins_url('css/css.css', __FILE__));
	wp_enqueue_style('sa-vc-promo-box-css');
	
	
	$svc_promo_id = rand(10,7000);
	ob_start();
	$link_target = $link_target;
	$lt = '';
	if($link_target == 'sw'){
		$lt = 'target="_self"';
	}elseif($link_target == 'nw'){
		$lt = 'target="_blank"';
	}
if($skin_type == 's1'){?>
<style type="text/css">
<?php 
	if($align){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box{ text-align: <?php echo $align;?>;}
	<?php }
	if($bg_mask_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box:after{ background:<?php echo $bg_mask_color;?>; }
	<?php }
	if($bg_mask_hover_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box:hover:before{background:<?php echo $bg_mask_hover_color;?>;}
	<?php }
	if($title_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover h4{ color: <?php echo $title_color?>;}
	<?php }
	if($title_hover_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box:hover .campaign-hover h4{ color: <?php echo $title_hover_color?>;}
	<?php }
	if($icon_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover>i {color: <?php echo $icon_color?>;border: 2px solid <?php echo $title_color?>;}
	<?php }
	if($icon_hover_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box:hover .campaign-hover>i {color: <?php echo $icon_hover_color?>;border: 2px solid <?php echo $icon_hover_color?>;}
	<?php }
	if($title_size){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover h4{ font-size: <?php echo $title_size?>px; line-height: <?php echo $title_size?>px;}
	<?php }
	if($desc_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover p{color:<?php echo $desc_color?>;}
	<?php }
	if($desc_hover_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box:hover .campaign-hover p{color:<?php echo $desc_hover_color?>;}
	<?php }
	if($description_size){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover p{font-size:<?php echo $description_size?>px; line-height: <?php echo $description_size?>px;}
	<?php }
	if($link_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover a.read-more{ color:<?php echo $link_color?>;}
	<?php }
	if($link_hover_color){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover a.read-more:hover{ color:<?php echo $link_hover_color?>;}
	<?php }
	if($link_size){?>
	.promo_s1.campaign-section.promo_s1_<?php echo $svc_promo_id;?> .campaign-box .campaign-hover a.read-more{ font-size:<?php echo $link_size?>px;}
	<?php }?>
</style>
<?php }?>
<?php if($skin_type == 's1'){?>
<section class="campaign-section promo_s1 <?php echo $svc_class;?> promo_s1_<?php echo $svc_promo_id;?>">
	<div>
	    <div class="campaign-box">
	        <div class="campaign-hover">
	        	<?php if($icon){?>
	            	<i class="fa <?php echo $icon;?>"></i>
	            <?php }
	            if($title){?>
	            	<h4><?php echo $title;?></h4>
	            <?php }
	            if($description){?>
	            	<p><?php echo $description;?></p>
	            <?php }
	            if($link){?>
            		<a href="<?php echo $link;?>" class="read-more" <?php echo $lt;?>><?php if($link_text == ''){ _e('Read More','js_composer');}else{ _e($link_text,'js_composer');}?> <i class="fa fa-long-arrow-right"></i></a>
	            <?php }?>
	        </div>
	        <?php echo wp_get_attachment_image( $image, $thumb_size,false,array('class' => 'sa_vc_image') );?>
	    </div>
	</div>
</section>
<?php }
if($skin_type == 's2'){?>
<style type="text/css">
	<?php if($bg_mask_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal{ background:<?php echo $bg_mask_color;?>; }
	<?php }
	if($bg_mask_hover_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal:hover{background:<?php echo $bg_mask_hover_color;?>;}
	<?php }
	if($title_size && $title_size != '16'){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table .table-title{ font-size:<?php echo $title_size;?>px;}
	<?php }
	if($title_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table .table-title{ color:<?php echo $title_color;?>;}
	<?php }
	if($title_hover_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table:hover .table-title{ color:<?php echo $title_hover_color;?>;}
	<?php }
	if($description_size){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table p {font-size:<?php echo $description_size?>;}
	<?php }
	if($desc_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table p {color:<?php echo $desc_color?>;}
	<?php }
	if($desc_hover_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table:hover p {color:<?php echo $desc_hover_color?>;}
	<?php }
	if($icon_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table i{ color: <?php echo $icon_color;?>;}
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal .btn-colored{ background: <?php echo $icon_color;?>;}
	<?php }
	if($icon_hover_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table:hover i{ color: <?php echo $icon_hover_color;?>;}
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal:hover .btn-colored{ background: <?php echo $icon_hover_color;?>;}
	<?php }
	if($link_size && $link_size != '14'){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal .btn-colored{ font-size: <?php echo $link_size;?>;}
	<?php }
	if($link_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal .btn-colored{ color: <?php echo $link_color;?>;}
	<?php }
	if($link_hover_color){?>
	.promo_s2.campaign-section.promo_s2_<?php echo $svc_promo_id;?> .pricing-table.table-horizontal:hover .btn-colored{ color: <?php echo $link_hover_color;?>;}
	<?php }?>
</style>
<?php }
if($skin_type == 's2'){?>
<section class="campaign-section promo_s2 <?php echo $svc_class;?> promo_s2_<?php echo $svc_promo_id;?>">
	<div class="pricing-table table-horizontal maxwidth400">
	    <div class="sa-row">
	      <div class="sa-col-sm-6 sa-text-center sa-icon">
	      	<?php if($image){
	      	echo wp_get_attachment_image( $image, $thumb_size,false,array('class' => 'sa_vc_image') );
	      	}else if($icon){?>
	        <div class="table-price text-white"><i class="fa <?php echo $icon;?>"></i></div>
	        <?php }
	        if($title){?>
	        <h6 class="table-title text-green"><?php echo $title;?></h6>
	        <?php }?>
	      </div>
	      <div class="sa-col-sm-6 sa-text-<?php echo $align;?>">
	      <?php if($description){?>
	        <p class="sa-text-left"><?php echo $description;?></p>
	      <?php }
	      if($link){?>
	    	<a href="<?php echo $link;?>" class="btn btn-colored btn-green btn-sm mt-15 pl-20 pr-20" <?php echo $lt;?>><?php if($link_text == ''){ _e('Read More','js_composer');}else{ _e($link_text,'js_composer');}?></a>
	      <?php }?>
	      </div>
	    </div>
	 </div>
</section>
<?php }?>
<?php
	$message = ob_get_clean();
	return $message;
}
?>
