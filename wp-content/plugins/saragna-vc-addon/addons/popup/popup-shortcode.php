<?php
function sa_vc_youtube_popup_shortcode($attr,$content=null){
	extract(shortcode_atts( array(
		'svc_type' => 'image',
		'image' => '',
		'thumb_size' => 'full',
		'text' => '',
		'video_url' => '',
		'autoplay' => '',
		'svc_class' => ''
	), $attr));
	
	wp_enqueue_script('sa-vc-megnific-js');
	
	wp_register_style( 'sa-vc-popup-css', plugins_url('css/css.css', __FILE__));
	wp_enqueue_style( 'sa-vc-megnific-css');
	wp_enqueue_style('sa-vc-popup-css');
	$popup_rand = rand(0,5000);
	ob_start();
?>
<div class="sa_vc_popup <?php echo $svc_class;?>">
	<a href="<?php echo $video_url;?>" class="popup-youtube popup-youtube-<?php echo $popup_rand;?>">
	<?php if($svc_type == 'image'){?>
		<?php echo wp_get_attachment_image( $image, $thumb_size,false,array('class' => 'sa_vc_popup_image') );?>
	<?php }else{
		echo $text;
	}?>
	</a>
</div>
<script type="text/javascript">
jQuery(function($){
	jQuery('.popup-youtube-<?php echo $popup_rand;?>').magnificPopup({
	  type: 'iframe',
	  mainClass: 'mfp-fade',
	  preloader: false,
	  closeBtnInside:false,
	  iframe: {
		 patterns: {
		   youtube: {
			index: 'youtube.com', 
			id: 'v=', 
			src: '//www.youtube.com/embed/%id%?rel=0&autoplay=<?php echo ($autoplay == 'yes') ? '1' : '0';?>'
		   }
		 }
	   }
	});
});
</script>
<?php 
	$message = ob_get_clean();
	return $message;
}

function sa_vc_vimeo_popup_shortcode($attr,$content=null){
	extract(shortcode_atts( array(
		'svc_type' => 'image',
		'image' => '',
		'thumb_size' => 'full',
		'text' => '',
		'video_url' => '',
		'autoplay' => '',
		'svc_class' => ''
	), $attr));
	
	wp_enqueue_script('sa-vc-megnific-js');
	
	wp_register_style( 'sa-vc-popup-css', plugins_url('css/css.css', __FILE__));
	wp_enqueue_style( 'sa-vc-megnific-css');
	wp_enqueue_style('sa-vc-popup-css');
	$popup_rand = rand(0,5000);
	ob_start();
?>
<div class="sa_vc_popup <?php echo $svc_class;?>">
	<a href="<?php echo $video_url;?>" class="popup-vimeo popup-vimeo-<?php echo $popup_rand;?>">
	<?php if($svc_type == 'image'){?>
		<?php echo wp_get_attachment_image( $image, $thumb_size,false,array('class' => 'sa_vc_popup_image') );?>
	<?php }else{
		echo $text;
	}?>
	</a>
</div>
<script type="text/javascript">
jQuery(function($){
	jQuery('.popup-vimeo-<?php echo $popup_rand;?>').magnificPopup({
	  type: 'iframe',
	  mainClass: 'mfp-fade',
	  preloader: false,
	  closeBtnInside:false,
	  iframe: {
		 patterns: {
		   vimeo: {
			index: 'vimeo.com', 
			id: '/',
			src: '//player.vimeo.com/video/%id%?autoplay=<?php echo ($autoplay == 'yes') ? '1' : '0';?>'
		   }
		 }
	   }
	});
});
</script>
<?php 
	$message = ob_get_clean();
	return $message;
}
?>
