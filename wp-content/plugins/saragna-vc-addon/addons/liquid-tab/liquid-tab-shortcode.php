<?php 
function sa_vc_liquid_tab_shortcode($attr,$content=null){
	extract(shortcode_atts( array(
		'tabs_position' => 'top',
		'tabs_align' => 'Left',
		'in_effect' => '',
		'out_effect' => '',
		'auto_height' => '1',
		'auto_slide' => '0',
		'auto_slide_intv' => '',
		'title_bgcolor' => '',
		'a_title_bgcolor' => '',
		'title_color' => '',
		'a_title_color' => '',
		'content_bgcolor' => ''
	), $attr));
	wp_register_style( 'sa-vc-liquid-slider-css', plugins_url('css/liquid-slider.css', __FILE__));
  	wp_enqueue_style( 'sa-vc-liquid-slider-css' );
	wp_register_style( 'sa-vc-liquid-animate-css', plugins_url('css/animate.min.css', __FILE__));
  	wp_enqueue_style( 'sa-vc-liquid-animate-css' );	
	
  	wp_enqueue_script('sa-vc-liquid-slider-js');
  	wp_enqueue_script('sa-vc-easing-js');
  	wp_enqueue_script('sa-vc-touchSwipe-js');
		
	ob_start();
	$tab_rand = rand(0,2000);?>
	<div class="ltab_<?php echo $tab_rand;?>">
	<div id="svc-tab-slider<?php echo $tab_rand;?>" class="liquid-slider">
    <?php echo wpb_js_remove_wpautop($content,true); // fix unclosed/unwanted paragraph tags in $content?>
    </div>
    </div>
	<style type="text/css">
	<?php if($tabs_align == 'Right'){?>
		.ltab_<?php echo $tab_rand;?> #svc-tab-slider<?php echo $tab_rand;?>-nav-ul{ float: right !important;}
	<?php }?>
	<?php if($tabs_align == 'Center' && $tabs_position == 'top'){?>
		.ltab_<?php echo $tab_rand;?> .ls-nav{ text-align: center; margin-bottom: -7px;}
		.ltab_<?php echo $tab_rand;?> #svc-tab-slider<?php echo $tab_rand;?>-nav-ul{ float: none !important; display: inline-block !important;}
	<?php }?>
	<?php if($tabs_align == 'Center' && $tabs_position == 'bottom'){?>
		.ltab_<?php echo $tab_rand;?> .ls-nav{ text-align: center; margin-top: -7px;}
		.ltab_<?php echo $tab_rand;?> #svc-tab-slider<?php echo $tab_rand;?>-nav-ul{ float: none !important; display: inline-block !important;}
	<?php }?>
	#svc-tab-slider<?php echo $tab_rand;?>-wrapper .ls-nav a{ background:<?php echo $title_bgcolor;?>;color:<?php echo $title_color;?>}
	#svc-tab-slider<?php echo $tab_rand;?>-wrapper .ls-nav .current a{ background:<?php echo $a_title_bgcolor;?>; color:<?php echo $a_title_color;?>}
	#svc-tab-slider<?php echo $tab_rand;?>{ background:<?php echo $content_bgcolor;?>}
	#svc-tab-slider<?php echo $tab_rand;?>-nav-ul{text-indent:0 !important; padding-left:0 !important;}
	</style>
    <script>
		jQuery(document).ready(function(e) {
		   jQuery('#svc-tab-slider<?php echo $tab_rand;?>').liquidSlider({
		   		dynamicTabsPosition:'<?php echo $tabs_position;?>',
		   		autoSlide:<?php echo ($auto_slide == '1') ? 'true' : 'false';?>,
				autoSlideInterval:<?php echo ($auto_slide_intv != '') ? $auto_slide_intv : '6e3';?>,
		   		autoHeight:<?php echo ($auto_height == '1') ? 'true' : 'false';?>,
				<?php if($in_effect != '' && $out_effect != ''){?>
		   		slideEaseFunction:'animate.css',
		   		animateIn:"<?php echo $in_effect;?>",
					animateOut:"<?php echo $out_effect;?>",
				callback: function() {
					var self = this;
					jQuery('.svc-tab-slider<?php echo $tab_rand;?>-panel').each(function() {
					  	jQuery(this).removeClass('animated ' + self.options.animateIn);
					});
				}
				<?php }?>
		   }); 
		});
		</script>			
<?php
	$message = ob_get_clean();
	return $message;
}
?>