jQuery(document).ready(function ($) {
    $(function () {
        var records = 5;
        var pageCurrent = 1;
        var max = $('.line-news').length;
        var totalPage = Math.ceil(max / records);
        elementShowTable(0, 5);
        $('.btn-previous').click(function (e) {
            e.preventDefault();
            process($(this), false);
        });
        $('.btn-next').click(function (e) {
            e.preventDefault();
            process($(this), true);
        });

        function process(element, next) {
            if (next) {
                updatePageCurrent(true);
            }
            else
            {
                updatePageCurrent(false);
            }
            elementShowTable();
        }

        function elementShowTable() {
            var end = pageCurrent * records;
            var start = (pageCurrent - 1) * records;
            $.each($('.widget-body .line-news'), function (index, element) {
                if (index < end && index >= start)
                    $(this).show();
                else
                    $(this).hide();
            });
        }

        function updatePageCurrent(add) {
            if(add)
            {
                pageCurrent++;
                if(pageCurrent > totalPage)
                    pageCurrent = totalPage;
            }

            else
            {
                pageCurrent--;
                if(pageCurrent < 1)
                    pageCurrent = 1;
            }
        }
    });
});