jQuery(function($) {
//worker hover handler
	var worker_els = $('.worker');
	for(var i=0, len=worker_els.length; i<len; i++) {
		worker_hover_handler(worker_els.eq(i));
	}
	
	function worker_hover_handler(el) {
		var hover_content = el.find('.worker-over-hover');
		el.on('mouseenter', function(e) {
			hover_content.stop().slideDown();
		})
		.on('mouseleave', function(e) {
			hover_content.stop().slideUp();
		});
	}
});