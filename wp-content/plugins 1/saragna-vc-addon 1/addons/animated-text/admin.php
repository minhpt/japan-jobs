<?php
include('animated-text-shortcode.php');
if(!class_exists('sa_vc_animated_text_layout'))
{
	class sa_vc_animated_text_layout
	{
		function __construct()
		{
			add_action('admin_init',array($this,'sa_vc_animated_text_layout_init'));
			add_shortcode('sa_vc_animate_text','sa_vc_animated_text_layout_shortcode');
		}
		function sa_vc_animated_text_layout_init(){
			$text_effect = 'flash bounce shake tada swing wobble pulse flip flipInX flipOutX flipInY flipOutY fadeIn fadeInUp fadeInDown fadeInLeft fadeInRight fadeInUpBig fadeInDownBig fadeInLeftBig fadeInRightBig fadeOut fadeOutUp fadeOutDown fadeOutLeft fadeOutRight fadeOutUpBig fadeOutDownBig fadeOutLeftBig fadeOutRightBig bounceIn bounceInDown bounceInUp bounceInLeft bounceInRight bounceOut bounceOutDown bounceOutUp bounceOutLeft bounceOutRight rotateIn rotateInDownLeft rotateInDownRight rotateInUpLeft rotateInUpRight rotateOut rotateOutDownLeft rotateOutDownRight rotateOutUpLeft rotateOutUpRight hinge rollIn rollOut';
			$in_out_array = explode(' ',$text_effect);
			$text_sequence = array('sequence','reverse','sync','shuffle');
			if(function_exists('vc_map')){
				vc_map( array(
					"name" => __('Animated Text','js_composer'),
					"base" => 'sa_vc_animate_text',
					"icon" => 'vc_animate_text',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Place Animated Text' ),
					"params" => array(
						array(
							'type' => 'dropdown',
							'heading' => __('Select Type','js_composer'),
							'param_name' => 'stype',
							'value' => array('Singal Sentence or Word' => '1','Sentence List' => '2'),
							'description' => __('Select Animated Text type.','js_composer')
						),
						array(		
							'type' => 'textfield',
							'heading' => __('ID','js_composer'),
							'param_name' => 'uid',
							'description' => __('Unique ID that will be used in the custom css.','js_composer')
						),
						array(		
							'type' => 'textarea',
							'heading' => __('Enter Text','js_composer'),
							'param_name' => 'text',
							'description' => __('Enter Animated Text Words.If you select type Sentence List write each sentence in a "|" seprate.','js_composer')
						),
						array(
							'type' => 'dropdown',
							'heading' => __('Effect','js_composer'),
							'param_name' => 'in_effect',
							'description' => __('Select Animated Text In Effect.','js_composer'),
							'value' => $in_out_array,
							'group' => __( 'In Effect', 'js_composer' ),
						),
						array(
							'type' => 'dropdown',
							'heading' => __('Sequence','js_composer'),
							'param_name' => 'in_sequence',
							'description' => __('Select Animated Text in sequence.','js_composer'),
							'value' => $text_sequence,
							'group' => __( 'In Effect', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __('Delay Scale','js_composer'),
							'param_name' => 'in_delay_scale',
							'description' => __('Set the delay factor applied to each consecutive character.','js_composer'),
							'value' => '1.5',
							'group' => __( 'In Effect', 'js_composer' ),
						),
						array(
							'type' => 'textfield',
		
							'heading' => __('Delay','js_composer'),
		
							'param_name' => 'in_delay',
		
							'description' => __('Set the delay between each character.','js_composer'),
							
							'value' => '50',
							
							'group' => __( 'In Effect', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textarea',
		
							'heading' => __('Callback In','js_composer'),
		
							'param_name' => 'in_callback',
		
							'description' => __("callback that executes once the 'in' animation has finished.<br/>Example:alert('in animation end.');",'js_composer'),
							
							'group' => __( 'In Effect', 'js_composer' ),
		
						),
						
						array(
		
							'type' => 'dropdown',
		
							'heading' => __('Effect','js_composer'),
		
							'param_name' => 'out_effect',
		
							'description' => __('Select Animated Text Out Effect.','js_composer'),
							
							'value' => $in_out_array,
							
							'group' => __( 'Out Effect', 'js_composer' ),
		
						),
						array(
		
							'type' => 'dropdown',
		
							'heading' => __('Sequence','js_composer'),
		
							'param_name' => 'out_sequence',
		
							'description' => __('Select Animated Text in sequence.','js_composer'),
							
							'value' => $text_sequence,
							
							'group' => __( 'Out Effect', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Delay Scale','js_composer'),
		
							'param_name' => 'out_delay_scale',
		
							'description' => __('Set the delay factor applied to each consecutive character.','js_composer'),
							
							'value' => '1.5',
							
							'group' => __( 'Out Effect', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Delay','js_composer'),
		
							'param_name' => 'out_delay',
		
							'description' => __('Set the delay between each character.','js_composer'),
							
							'value' => '50',
							
							'group' => __( 'Out Effect', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textarea',
		
							'heading' => __('Callback In','js_composer'),
		
		
							'param_name' => 'out_callback',
		
							'description' => __("callback that executes once the 'out' animation has finished.<br/>Example:alert('out animation end.');",'js_composer'),
							
							'group' => __( 'Out Effect', 'js_composer' ),
		
						),
						array(
		
							'type' => 'dropdown',
		
							'heading' => __('Loop','js_composer'),
		
							'param_name' => 'loop',
		
							'description' => __('Enable text animation looping.','js_composer'),
							
							'value'	=> array('No' => '0','Yes' => '1'),
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Min Display Time','js_composer'),
		
							'param_name' => 'min_display_time',
		
							'description' => __('Set the minimum display time for each text before it is replaced - in milliseconds(Sentence List only).','js_composer'),
							
							'value' => '2000',
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Initial Delay','js_composer'),
		
							'param_name' => 'init_delay',
		
							'description' => __('sets the initial delay before starting the animation - in miliseconds.','js_composer'),
							
							'value' => '0',
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'dropdown',
		
							'heading' => __('Auto Start','js_composer'),
		
							'param_name' => 'auto_start',
		
							'description' => __('set whether or not to automatically start animating.','js_composer'),
							
							'value'	=> array('Yes' => '1','No' => '0'),
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'dropdown',
		
							'heading' => __('Type','js_composer'),
		
							'param_name' => 'type',
		
							'description' => __('set the type of token to animate.','js_composer'),
							
							'value'	=> array('Char' => 'char','Word' => 'word'),
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'dropdown',
		
							'heading' => __('Viewport Checker','js_composer'),
		
							'param_name' => 'viewport',
		
							'description' => __('shows the animation only when the text is visible on the screen.','js_composer'),
							
							'value'	=> array('Yes' => '1','No' => '0'),
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textarea',
		
							'heading' => __('Custom CSS Style','js_composer'),
		
							'param_name' => 'css',
		
							'description' => __('use to change the color (and/or other propery) of a sentence, word or character.<br/>Color Examples:(#vc-animate-text-{ID})<br/>//Change the color of the whole sentence<br/>#vc-animate-text-{ID}{ color: #ff0000;}<br/>//Change the color of the second word<br/>#vc-animate-text-{ID} .word2{ color: #ff0000;}<br/>//change the color of the third character of the third word<br/>#vc-animate-text-{ID} .word3 .char3{ color: #ff0000;}<br/>//change the color of all first character<br/>#vc-animate-text-{ID} .char1{ color: #ff0000;}','js_composer'),
							
							'group' => __( 'General Options', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Compressor','js_composer'),
		
							'param_name' => 'compressor',
		
							'description' => __('Decimal number. From 0.1 to 5.0','js_composer'),
							
							'value' => '1.0',
							
							'group' => __( 'Fit Text - Responsive Feature', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Min. Font Size','js_composer'),
		
							'param_name' => 'min_font_size',
		
							'description' => __('value in pixels. it is not necessary to put the "px".','js_composer'),
							
							'value' => '14',
							
							'group' => __( 'Fit Text - Responsive Feature', 'js_composer' ),
		
						),
						array(
		
							'type' => 'textfield',
		
							'heading' => __('Max. Font Size','js_composer'),
		
							'param_name' => 'max_font_size',
		
							'description' => __('value in pixels. it is not necessary to put the "px".','js_composer'),
							
							'value' => '22',
							
							'group' => __( 'Fit Text - Responsive Feature', 'js_composer' ),
		
						),
						
						
		
					)

				) );
			}

		}

	}
	
	
	//instantiate the class
	$sa_vc_animated_text_layout = new sa_vc_animated_text_layout;
}
