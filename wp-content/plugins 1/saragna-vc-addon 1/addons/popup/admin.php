<?php
include('popup-shortcode.php');
if(!class_exists('sa_vc_popup'))
{
	class sa_vc_popup
	{
		function __construct(){
			add_action('admin_init',array($this,'sa_vc_popup_init'));
			add_shortcode('sa_vc_youtube_popup','sa_vc_youtube_popup_shortcode');
			add_shortcode('sa_vc_vimeo_popup','sa_vc_vimeo_popup_shortcode');
		}
		function sa_vc_popup_init(){

			if(function_exists('vc_map')){
				vc_map( array(
					"name" => __('YouTube Video Popup','js_composer'),		
					"base" => 'sa_vc_youtube_popup',		
					"icon" => 'vc_youtube_popup_logo',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set youtube popup.','js_composer' ),
					"params" => array(
						array(
							"type" => "dropdown",
							"heading" => __("type" , 'js_composer' ),
							"param_name" => "svc_type",
							"value" =>array(
								__("Image Click", 'js_composer' )=>"image",
								__("Text Click", 'js_composer' )=>"text"
								),
							'std' => 'image',
							"description" => __("Choose Click Event type.", 'js_composer' ),
						),
						array(
							"type" => "attach_image",
							"heading" => __("Image" , 'js_composer' ),
							"param_name" => "image",
							'dependency' => array('element' => 'svc_type','value' => 'image'),
							"description" => __("select image.", 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Thumbnail size', 'js_composer' ),
							'param_name' => 'thumb_size',
							'dependency' => array('element' => 'svc_type','value' => 'image'),
							'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme..', 'js_composer' )
						),
						array(
							"type" => "textfield",
							"heading" => __("Enter Text" , 'js_composer' ),
							"param_name" => "text",
							'dependency' => array('element' => 'svc_type','value' => 'text'),
							"description" => __("Enter Text.", 'js_composer' ),
						),
						array(
							"type" => "textfield",
							"heading" => __("Video URL" , 'js_composer' ),
							"param_name" => "video_url",
							"description" => __("Enter Video URL. like: https://www.youtube.com/watch?v=HcgJRQWxKnw", 'js_composer' ),
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Popup video Autoplay', 'js_composer' ),
							'param_name' => 'autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'description' => __( 'Show autoplay after click.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						)
					)
				) );
				
				vc_map( array(
					"name" => __('Vimeo Video Popup','js_composer'),		
					"base" => 'sa_vc_vimeo_popup',		
					"icon" => 'vc_vimeo_popup_logo',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set Vimeo popup.','js_composer' ),
					"params" => array(
						array(
							"type" => "dropdown",
							"heading" => __("type" , 'js_composer' ),
							"param_name" => "svc_type",
							"value" =>array(
								__("Image Click", 'js_composer' )=>"image",
								__("Text Click", 'js_composer' )=>"text"
								),
							'std' => 'image',
							"description" => __("Choose Click Event type.", 'js_composer' ),
						),
						array(
							"type" => "attach_image",
							"heading" => __("Image" , 'js_composer' ),
							"param_name" => "image",
							'dependency' => array('element' => 'svc_type','value' => 'image'),
							"description" => __("select image.", 'js_composer' ),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Thumbnail size', 'js_composer' ),
							'param_name' => 'thumb_size',
							'dependency' => array('element' => 'svc_type','value' => 'image'),
							'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme..', 'js_composer' )
						),
						array(
							"type" => "textfield",
							"heading" => __("Enter Text" , 'js_composer' ),
							"param_name" => "text",
							'dependency' => array('element' => 'svc_type','value' => 'text'),
							"description" => __("Enter Text.", 'js_composer' ),
						),
						array(
							"type" => "textfield",
							"heading" => __("Video URL" , 'js_composer' ),
							"param_name" => "video_url",
							"description" => __("Enter Video URL. like: https://vimeo.com/139422163", 'js_composer' ),
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Popup video Autoplay', 'js_composer' ),
							'param_name' => 'autoplay',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'description' => __( 'Show autoplay after click.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						)
					)
				) );
				
			}

		}

	}
	
	
	//instantiate the class
	$sa_vc_popup = new sa_vc_popup;
}
