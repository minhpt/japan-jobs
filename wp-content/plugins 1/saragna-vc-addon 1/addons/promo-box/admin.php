<?php
include('promo-box-shortcode.php');
if(!class_exists('sa_vc_promo_box_layout'))
{
	class sa_vc_promo_box_layout
	{
		function __construct()
		{
			add_action('admin_init',array($this,'sa_vc_promo_box_layout_init'));
			add_shortcode('sa_vc_promo_box','sa_vc_promo_box_layout_shortcode');
		}
		function sa_vc_promo_box_layout_init()
		{

			if(function_exists('vc_map'))
			{
				vc_map( array(
					"name" => __('Promo Box','js_composer'),		
					"base" => 'sa_vc_promo_box',		
					"icon" => 'vc_promo_box',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Promo box.','js_composer' ),
					"params" => array(
						array(
							"type" => "dropdown",
							"heading" => __("Skin type" , 'js_composer' ),
							"param_name" => "skin_type",
							"value" =>array(
								__("Style1", 'js_composer' )=>"s1",
								__("Style2", 'js_composer' )=>"s2",
								),
							"description" => __("Choose skin type for Promo box Layout.", 'js_composer' ),
						),
						array(
							'type' => 'attach_image',
							'heading' => __( 'Select Image', 'js_composer' ),
							'param_name' => 'image',
							'dependency' => array('element' => 'skin_type','value' => array('s1','s2')),
							'description' => __( 'Select Image', 'js_composer' )
						),						
						array(			    			   
							'type' => 'textfield',
							"heading" => __("Icon",'js_composer'),
							"param_name" => "icon",		
							'description' => __( 'Enter font icon awesome .eg: fa-facebook <a href="http://fortawesome.github.io/Font-Awesome/icons/">click here.</a>', 'js_composer' ),
						 ), 
						array(
							'type' => 'textfield',
							'heading' => __( 'Title', 'js_composer' ),
							'param_name' => 'title',
							'holder' => 'div',
							'description' => __( 'Set Promo Box Title.', 'js_composer' )
						),
						array(
							'type' => 'textarea',
							'heading' => __( 'Sort Description', 'js_composer' ),
							'param_name' => 'description',
							'description' => __( 'Set Promo Box Description.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Link', 'js_composer' ),
							'param_name' => 'link',
							'description' => __( 'Set Promo Box Link.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Link Text', 'js_composer' ),
							'param_name' => 'link_text',
							'description' => __( 'Set Promo Box Link Text.Default : Read more <i class="fa fa-angle-double-right"></i>', 'js_composer' )
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Link target', 'js_composer' ),
							'param_name' => 'link_target',
							'value' => array('Same Window' => 'sw','New Window' => 'nw'),
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Thumbnail size', 'js_composer' ),
							'param_name' => 'thumb_size',
							'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme.', 'js_composer' )
						),
						array(
							"type" => "dropdown",
							"heading" => __("Content Align" , 'js_composer' ),
							"param_name" => "align",
							"value" =>array(
								__("Left", 'js_composer' )=>"left",
								__("Center", 'js_composer' )=>"center",
								__("Right", 'js_composer' )=>"right"
								),
							'std' => 'left',
							"description" => __("set Content Text align.", 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Title font Size', 'js_composer' ),
							'param_name' => 'title_size',
							'value' => '16',
							'min' => 0,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'description' => __( 'set Title font size.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description font Size', 'js_composer' ),
							'param_name' => 'description_size',
							'value' => '0',
							'min' => 0,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'description' => __( 'set Description font size. default:0 mince inherit and apply your theme default content size.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'Link font Size', 'js_composer' ),
							'param_name' => 'link_size',
							'value' => '14',
							'min' => 0,
							'max' => 1000,
							'suffix' => 'px',
							'step' => 1,
							'description' => __( 'set link font size.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'holder' => 'div',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Background mask Color', 'js_composer' ),
							'param_name' => 'bg_mask_color',
							'description' => __( 'set Background mask color.', 'js_composer' ),
							'dependency' => array('element' => 'skin_type','value' => array('s1','s2')),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Background mask hover Color', 'js_composer' ),
							'param_name' => 'bg_mask_hover_color',
							'description' => __( 'set Background mask hover color.', 'js_composer' ),
							'dependency' => array('element' => 'skin_type','value' => array('s1','s2')),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title or icon Color', 'js_composer' ),
							'param_name' => 'title_color',
							'description' => __( 'set title or icon color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Title or icon hover Color', 'js_composer' ),
							'param_name' => 'title_hover_color',
							'description' => __( 'set title or icon hover color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Icon Color', 'js_composer' ),
							'param_name' => 'icon_color',
							'description' => __( 'set Icon color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Icon hover Color', 'js_composer' ),
							'param_name' => 'icon_hover_color',
							'description' => __( 'set Icon hover color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Description Color', 'js_composer' ),
							'param_name' => 'desc_color',
							'description' => __( 'set Description color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Description hover Color', 'js_composer' ),
							'param_name' => 'desc_hover_color',
							'description' => __( 'set Description hover color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Link Color', 'js_composer' ),
							'param_name' => 'link_color',
							'description' => __( 'set Link color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Link hover Color', 'js_composer' ),
							'param_name' => 'link_hover_color',
							'description' => __( 'set Link hover color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),

					)
				) );
				
			}

		}

	}
	
	
	//instantiate the class
	$sa_vc_promo_box_layout = new sa_vc_promo_box_layout;
}
