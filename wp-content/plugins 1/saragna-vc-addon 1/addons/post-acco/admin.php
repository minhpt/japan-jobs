<?php
include('post-acco-shortcode.php');
if(!class_exists('sa_vc_post_acco_layout'))
{
	class sa_vc_post_acco_layout
	{
		function __construct()
		{
			add_action('admin_init',array($this,'sa_vc_post_acco_layout_init'));
			add_shortcode('sa_vc_post_acco','sa_vc_post_acco_layout_shortcode');
		}
		function sa_vc_post_acco_layout_init()
		{

			if(function_exists('vc_map'))
			{
				vc_map( array(
					"name" => __('Post Accordion','js_composer'),		
					"base" => 'sa_vc_post_acco',		
					"icon" => 'vc_post_acco_logoo',
					"category" => __('Saragna Addons','js_composer'),
					'description' => __( 'Set your Post Accordion.','js_composer' ),
					"params" => array(
						array(
							'type' => 'textfield',
							'heading' => __( 'Title', 'js_composer' ),
							'param_name' => 'title',
							'holder' => 'div',
							'description' => __( 'Set Post Accordion Title.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Sort Description', 'js_composer' ),
							'param_name' => 'top_description',
							'holder' => 'div',
							'description' => __( 'Set Top Sort Description.', 'js_composer' )
						),
						array(
							'type' => 'loop',
							'heading' => __('Build Post Query','js_composer'),
							'param_name' => 'query_loop',
							'settings' => array(
								'post_type' => array('value' => 'post'),
								'size' => array( 'hidden' => false, 'value' => 10 ),
								'order_by' => array( 'value' => 'date' ),
								'order' => array('value' => 'DESC')
							),
							'description' => __('Create WordPress loop, to populate content from your site.','js_composer')
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Featured Image Hide', 'js_composer' ),
							'param_name' => 'featured_hide',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'description' => __( 'Hide Featured Image. WWPPLLOOCCKKEERR..CCOOMM', 'js_composer' ),
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Meta Hide', 'js_composer' ),
							'param_name' => 'meta_hide',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'description' => __( 'Hide meta data.', 'js_composer' ),
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'Description Hide', 'js_composer' ),
							'param_name' => 'content_hide',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'description' => __( 'Hide post Content.', 'js_composer' ),
						),
						array(
							'type' => 'checkbox',
							'heading' => __( 'All Tab Close', 'js_composer' ),
							'param_name' => 'all_tab_close',
							'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
							'description' => __( 'Set all tab Close. Default First Tab open.', 'js_composer' ),
						),
						array(
							'type' => 'num',
							'heading' => __( 'Description Length', 'js_composer' ),
							'param_name' => 'content_length',
							'value' => '0',
							'min' => 0,
							'max' => 10000,
							'suffix' => 'words',
							'step' => 1,
							'description' => __( 'set content length.Default 0', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'Tab Padding Top/Bottom', 'js_composer' ),
							'param_name' => 'tab_padding',
							'value' => '0',
							'min' => 0,
							'max' => 10000,
							'suffix' => 'px',
							'step' => 1,
							'description' => __( 'set content length.Default 0 mince Default Work.', 'js_composer' )
						),
						array(
							'type' => 'num',
							'heading' => __( 'Content spacer Height', 'js_composer' ),
							'param_name' => 'spacer',
							'value' => '20',
							'min' => 0,
							'max' => 10000,
							'suffix' => 'rem',
							'step' => 1,
							'description' => __( 'set content spacer Height.Default 20.', 'js_composer' )
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Link target', 'js_composer' ),
							'param_name' => 'grid_link_target',
							'value' => array('Same Window' => 'sw','New Window' => 'nw'),
							'description' => __( 'set Read more link target', 'js_composer' )
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Title & Sort Description Position', 'js_composer' ),
							'param_name' => 'top_content_position',
							'value' => array('Top' => 'top','Bottom' => 'bottom'),
							'description' => __( 'set position of main title and sort description. Default : Top', 'js_composer' )
						),
						array(
							'type' => 'dropdown',
							'heading' => __( 'Tab Content Text Align', 'js_composer' ),
							'param_name' => 'tab_text_align',
							'value' => array('Left' => 'left','Center' => 'center','Right' => 'right'),
							'description' => __( 'set tab text align of title and meta. Default : Left', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Thumbnail size', 'js_composer' ),
							'param_name' => 'thumb_size',
							'description' => __( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Read More Translate', 'js_composer' ),
							'param_name' => 'read_more',
							'description' => __( 'set Translate for "Read More" text.default : Read More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Find more Translate', 'js_composer' ),
							'param_name' => 'find_more',
							'description' => __( 'set Translate for "Find More" text.default : Find More', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Find More Link', 'js_composer' ),
							'param_name' => 'find_more_link',
							'description' => __( 'Set Find more link.', 'js_composer' )
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'Extra class name', 'js_composer' ),
							'param_name' => 'svc_class',
							'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Background Color', 'js_composer' ),
							'param_name' => 'pbgcolor',
							'description' => __( 'set tab background color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Post Title Color', 'js_composer' ),
							'param_name' => 'tcolor',
							'description' => __( 'set tab Title color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Read more Button Color', 'js_composer' ),
							'param_name' => 'btncolor',
							'description' => __( 'set Read more button color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Read more Text Color', 'js_composer' ),
							'param_name' => 'btn_text_color',
							'description' => __( 'set Read more Text color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						),
						array(
							'type' => 'colorpicker',
							'heading' => __( 'Content Text Color', 'js_composer' ),
							'param_name' => 'content_text_color',
							'description' => __( 'set Content Text color.', 'js_composer' ),
							'group' => __('Color Setting', 'js_composer')
						)
					)
				) );
				
			}

		}

	}
	
	
	//instantiate the class
	$sa_vc_post_acco_layout = new sa_vc_post_acco_layout;
}
