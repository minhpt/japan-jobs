<?php
include('liquid-tab-shortcode.php');
if(!class_exists('sa_vc_liquid_tab'))
{
	class sa_vc_liquid_tab
	{
		function __construct()
		{
			add_action('admin_init',array($this,'sa_vc_liquid_tab_init'));
			add_shortcode('sa_vc_liquid_tab','sa_vc_liquid_tab_shortcode');
		}
		function sa_vc_liquid_tab_init()
		{
			if(function_exists('vc_map'))
			{
			
				$out_effect = array('','flash','bounce','shake','tada','swing','wobble','wiggle','pulse','flip','flipInX','flipInY','fadeOut','fadeOutUp','fadeOutDown','fadeOutLeft','fadeOutRight','fadeOutUpBig','fadeOutDownBig','fadeOutLeftBig','fadeOutRightBig','bounceOut','bounceOutDown','bounceOutUp','bounceOutLeft','bounceOutRight','rotateOut','rotateOutDownLeft','rotateOutDownRight','rotateOutUpLeft','rotateOutUpRight','lightSpeedOut','hinge','rollOut');
				$in_effect = array('','flash','bounce','shake','tada','swing','wobble','wiggle','pulse','flip','flipInX','flipInY','fadeIn','fadeInUp','fadeInDown','fadeInLeft','fadeInRight','fadeInUpBig','fadeInDownBig','fadeInLeftBig','fadeInRightBig','bounceIn','bounceInDown','bounceInUp','bounceInLeft','bounceInRight','rotateIn','rotateInDownLeft','rotateInDownRight','rotateInUpLeft','rotateInUpRight','lightSpeedIn','hinge','rollIn');
				vc_map( array(		
					"name" => __('Liquid Tab','js_composer'),		
					"base" => 'sa_vc_liquid_tab',		
					"icon" => 'vc_liquid_tab_icon',		
					"category" => __('Saragna Addons','js_composer'),		
					'description' => __( 'Place Responsive liquid tab','js_composer' ),		
					"params" => array(
						array(
							"type" => "textarea_html",
							"holder" => "div",
							"heading" => __("Tabs content, divide(wrap) each one with &lt;div class=&#039;liquid-tab&#039;&gt;&lt;/div&gt;, please edit in text mode:", "js_composer"),
							"param_name" => "content",
							"value" => __("<div class='liquid-tab'><h2 class='title'>Slide 1</h2><p>You have to wrap each tabs block in a div with class <strong>liquid-tab</strong>.</p>
							  <p>So you can put anything in it, like a image or video.</p></div>
							  <div class='liquid-tab'>
							  <h2 class='title'>Slide 2</h2>
								<p>Tab content 2, please edit it in the text editor.</p>
							  </div>
							  <div class='liquid-tab'>
							  <h2 class='title'>Slide 3</h2>
								<p>Tab content 3, please edit it in the text editor.</p>
								<p><a href='http://codecanyon.net/user/saragna?ref=saragna'>Visit my profile</a> for more works.</p>
							  </div>
							  <div class='liquid-tab'>
							  <h2 class='title'>Slide 4</h2>
								<p>Tab content 4, please edit it in the text editor.</p>
							  </div>", "js_composer"),
							  "description" => __("Enter content for each block here.", "js_composer") 
						),
						array(		
							'type' => 'dropdown',		
							'heading' => __('Tabs Position','js_composer'),		
							'param_name' => 'tabs_position',							
							'value' => array('top','bottom'),		
							'description' => __('Select tabs position.','js_composer')		
						),
						array(		
							'type' => 'dropdown',		
							'heading' => __('Tabs Align','js_composer'),		
							'param_name' => 'tabs_align',							
							'value' => array('Left','Center','Right'),		
							'description' => __('Select tabs position.','js_composer')		
						),
						array(		
							'type' => 'dropdown',		
							'heading' => __('In Effect','js_composer'),		
							'param_name' => 'in_effect',							
							'value' => $in_effect,		
							'description' => __('Select tab in effect.','js_composer')		
						),
						array(		
							'type' => 'dropdown',		
							'heading' => __('Out Effect','js_composer'),		
							'param_name' => 'out_effect',							
							'value' => $out_effect,		
							'description' => __('Select tab Out effect.','js_composer')		
						),
						array(		
							'type' => 'dropdown',		
							'heading' => __('Auto Height','js_composer'),
							'param_name' => 'auto_height',
							'value' => array('Yes' => '1','No' =>'0' ),
							'description' => __('Set auto Height for tabs.','js_composer')		
						),
						array(		
							'type' => 'dropdown',		
							'heading' => __('Auto Slide','js_composer'),
							'param_name' => 'auto_slide',
							'value' => array('No' => '0','Yes' =>'1' ),
							'description' => __('Set auto Height for tabs.','js_composer')		
						),
						array(		
							'type' => 'textfield',		
							'heading' => __('Auto Slide Interval','js_composer'),		
							'param_name' => 'auto_slide_intv',
							'dependency' => array('element' => 'auto_slide','value' => '1'),
							'description' => __('set two tab auto Slide between interval. only integer value in milisecound.ex: 1000 = 1s ','js_composer')
						),
						array(		
							'type' => 'colorpicker',		
							'heading' => __('Tab Title Background Color','js_composer'),		
							'param_name' => 'title_bgcolor',		
							'description' => __('Color of the tab title background color.','js_composer'),
							'group' => __('Color Setting','js_composer')
						),
						array(		
							'type' => 'colorpicker',		
							'heading' => __('Active Tab Title Background Color','js_composer'),		
							'param_name' => 'a_title_bgcolor',		
							'description' => __('Color of the active tab title background color.','js_composer'),
							'group' => __('Color Setting','js_composer')
						),
						array(		
							'type' => 'colorpicker',		
							'heading' => __('Tab Title Color','js_composer'),		
							'param_name' => 'title_color',		
							'description' => __('Color of the tab title color.','js_composer'),
							'group' => __('Color Setting','js_composer')
						),
						array(		
							'type' => 'colorpicker',		
							'heading' => __('Active Tab Title Color','js_composer'),		
							'param_name' => 'a_title_color',		
							'description' => __('Color of the active tab title color.','js_composer'),
							'group' => __('Color Setting','js_composer')
						),
						array(		
							'type' => 'colorpicker',		
							'heading' => __('Content background Color','js_composer'),		
							'param_name' => 'content_bgcolor',		
							'description' => __('Color of the tab content background color.','js_composer'),
							'group' => __('Color Setting','js_composer')
						),
					)
				) );
			}
		}
		
	}
	//instantiate the class
	$sa_vc_liquid_tab = new sa_vc_liquid_tab;
}