<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'chuanseo_sanviec');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^vX1iuJ,T{&H~Ek`k]d/8YVEOq+^;Q 6#p1E`S#CnZouW]v!LSK}ikX<wD*F/E7%');
define('SECURE_AUTH_KEY',  'eGG7pU#|vZ.dKlU!SN33`Y?$!*8{XXNEf,RkNR/.S%ep?sCSBE.T=)pOLH(u^K>0');
define('LOGGED_IN_KEY',    'T@ ;SndC)I{h3a!!&H{Zo|Er=Lia|gDRm9?VD7[cJqdyuyZM!g0bts_B_JHI()pJ');
define('NONCE_KEY',        'IKEJ[cAqsZ!&}g3eBpJ7rpe..?GS;GVK;.xEOUmn|&)prO?*-#[z_;M)giiD ~jJ');
define('AUTH_SALT',        'rKNmM]d>6rfDimRm6qOjOm!q|uSgh9i0W~jPsuY0lu(_T~d&X>M-N?Y|n0`s+BZq');
define('SECURE_AUTH_SALT', 'pl-UP0B?k?_#2Cgais~<Yw-Rv.wx4(YIR|adt5(27kq4)duz^8nhN?:F,Xh6MfW-');
define('LOGGED_IN_SALT',   '~$[uC/5w6~|D};=qo9``kht>Vmb[E-ti5Lh2k#FGTS`n%9 *`[o2/maD8d;d`wO<');
define('NONCE_SALT',       'AP3-67DojE,e=WQ<XIj6A-5[>YO&ZwcoX/:O,~lM-]KW%]r@xkKlWALHSNX5OrY~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'chuanseo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
